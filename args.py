# -*- coding: utf-8 -*-
"""Playbook Args"""


class Args(object):
    """Playbook Args"""

    def __init__(self, parser):
        """Initialize class properties."""
        parser.add_argument('--tc_action', required=True)
        parser.add_argument('--indicator_type_1')
        parser.add_argument('--indicator_type')
        parser.add_argument('--indicator_type_input')
        parser.add_argument('--get_by')
        parser.add_argument('--delete_by')
        parser.add_argument('--associated_to')  # Retrieve by association
        parser.add_argument('--rating')  # Retrieve by association
        parser.add_argument('--confidence')  # Retrieve by association
        parser.add_argument('--indicator_entity')
        parser.add_argument('--indicator_value')
        parser.add_argument('--indicator_values')
        parser.add_argument('--indicator_entity_or_value')
        parser.add_argument('--owner', required=True)
        parser.add_argument('--owner_input')

        # Metadata
        parser.add_argument('--indicator_tag')

        # Flags
        parser.add_argument('--fail_on_error', action='store_true')
        parser.add_argument('--fail_on_no_results', action='store_true')

        # Registry Key
        parser.add_argument('--resource_name')
        parser.add_argument('--value_name')
        parser.add_argument('--value_type')
        parser.add_argument('--resource_names')
        parser.add_argument('--value_names')
        parser.add_argument('--value_types')

        # Host/Address
        parser.add_argument('--dns_active')
        parser.add_argument('--whois_active')

        # File
        parser.add_argument('--md5_hash')
        parser.add_argument('--sha1_hash')
        parser.add_argument('--sha256_hash')
        parser.add_argument('--file_size')
        parser.add_argument('--md5_hashes')
        parser.add_argument('--sha1_hashes')
        parser.add_argument('--sha256_hashes')
        parser.add_argument('--file_sizes')

        # Custom
        parser.add_argument('--custom_value_1')
        parser.add_argument('--custom_value_2')
        parser.add_argument('--custom_value_3')
        parser.add_argument('--custom_values_1')
        parser.add_argument('--custom_values_2')
        parser.add_argument('--custom_values_3')
