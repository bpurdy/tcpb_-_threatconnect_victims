# -*- coding: utf-8 -*-
"""Validate App outputs Class."""
# flake8: noqa
# pylint: disable=line-too-long


class Validate(object):
    """Validate base class for App output validation."""

    def __init__(self, validator):
        """Initialize class properties."""
        self.validator = validator
        self.validation_methods = {
            '#App:9876:tc.address.action!String': self.tc_address_action_string,
            '#App:9876:tc.address.active_locked!String': self.tc_address_active_locked_string,
            '#App:9876:tc.address.active!String': self.tc_address_active_string,
            '#App:9876:tc.address.attribute!TCEntityArray': self.tc_address_attribute_tcentityarray,
            '#App:9876:tc.address.confidence!String': self.tc_address_confidence_string,
            '#App:9876:tc.address.count!String': self.tc_address_count_string,
            '#App:9876:tc.address.date_added!String': self.tc_address_date_added_string,
            '#App:9876:tc.address.fail.count!String': self.tc_address_fail_count_string,
            '#App:9876:tc.address.false_positive_count!String': self.tc_address_false_positive_count_string,
            '#App:9876:tc.address.false_positive_last_reported!String': self.tc_address_false_positive_last_reported_string,
            '#App:9876:tc.address.id!String': self.tc_address_id_string,
            '#App:9876:tc.address.ip!String': self.tc_address_ip_string,
            '#App:9876:tc.address.json.raw!String': self.tc_address_json_raw_string,
            '#App:9876:tc.address.last_modified!String': self.tc_address_last_modified_string,
            '#App:9876:tc.address.last_observed!String': self.tc_address_last_observed_string,
            '#App:9876:tc.address.observation_count!String': self.tc_address_observation_count_string,
            '#App:9876:tc.address.owner_name!String': self.tc_address_owner_name_string,
            '#App:9876:tc.address.private_flag!String': self.tc_address_private_flag_string,
            '#App:9876:tc.address.rating!String': self.tc_address_rating_string,
            '#App:9876:tc.address.security_label!StringArray': self.tc_address_security_label_stringarray,
            '#App:9876:tc.address.source!String': self.tc_address_source_string,
            '#App:9876:tc.address.success.count!String': self.tc_address_success_count_string,
            '#App:9876:tc.address.tag!StringArray': self.tc_address_tag_stringarray,
            '#App:9876:tc.address!TCEntity': self.tc_address_tcentity,
            '#App:9876:tc.address.threat_assess_score!String': self.tc_address_threat_assess_score_string,
            '#App:9876:tc.address.web_link!String': self.tc_address_web_link_string,
            '#App:9876:tc.addresses.action!String': self.tc_addresses_action_string,
            '#App:9876:tc.addresses.active_locked!StringArray': self.tc_addresses_active_locked_stringarray,
            '#App:9876:tc.addresses.active!StringArray': self.tc_addresses_active_stringarray,
            '#App:9876:tc.addresses.confidence!StringArray': self.tc_addresses_confidence_stringarray,
            '#App:9876:tc.addresses.count!String': self.tc_addresses_count_string,
            '#App:9876:tc.addresses.date_added!StringArray': self.tc_addresses_date_added_stringarray,
            '#App:9876:tc.addresses.fail.count!String': self.tc_addresses_fail_count_string,
            '#App:9876:tc.addresses.false_positive_count!StringArray': self.tc_addresses_false_positive_count_stringarray,
            '#App:9876:tc.addresses.false_positive_last_reported!StringArray': self.tc_addresses_false_positive_last_reported_stringarray,
            '#App:9876:tc.addresses.id!StringArray': self.tc_addresses_id_stringarray,
            '#App:9876:tc.addresses.ip!StringArray': self.tc_addresses_ip_stringarray,
            '#App:9876:tc.addresses.json.raw!String': self.tc_addresses_json_raw_string,
            '#App:9876:tc.addresses.last_modified!StringArray': self.tc_addresses_last_modified_stringarray,
            '#App:9876:tc.addresses.last_observed!StringArray': self.tc_addresses_last_observed_stringarray,
            '#App:9876:tc.addresses.observation_count!StringArray': self.tc_addresses_observation_count_stringarray,
            '#App:9876:tc.addresses.owner_name!StringArray': self.tc_addresses_owner_name_stringarray,
            '#App:9876:tc.addresses.private_flag!StringArray': self.tc_addresses_private_flag_stringarray,
            '#App:9876:tc.addresses.rating!StringArray': self.tc_addresses_rating_stringarray,
            '#App:9876:tc.addresses.source!StringArray': self.tc_addresses_source_stringarray,
            '#App:9876:tc.addresses.success.count!String': self.tc_addresses_success_count_string,
            '#App:9876:tc.addresses!TCEntityArray': self.tc_addresses_tcentityarray,
            '#App:9876:tc.addresses.threat_assess_score!StringArray': self.tc_addresses_threat_assess_score_stringarray,
            '#App:9876:tc.addresses.web_link!StringArray': self.tc_addresses_web_link_stringarray,
            '#App:9876:tc.asn.action!String': self.tc_asn_action_string,
            '#App:9876:tc.asn.active_locked!String': self.tc_asn_active_locked_string,
            '#App:9876:tc.asn.active!String': self.tc_asn_active_string,
            '#App:9876:tc.asn.as_number!String': self.tc_asn_as_number_string,
            '#App:9876:tc.asn.attribute!TCEntityArray': self.tc_asn_attribute_tcentityarray,
            '#App:9876:tc.asn.confidence!String': self.tc_asn_confidence_string,
            '#App:9876:tc.asn.count!String': self.tc_asn_count_string,
            '#App:9876:tc.asn.date_added!String': self.tc_asn_date_added_string,
            '#App:9876:tc.asn.fail.count!String': self.tc_asn_fail_count_string,
            '#App:9876:tc.asn.false_positive_count!String': self.tc_asn_false_positive_count_string,
            '#App:9876:tc.asn.false_positive_last_reported!String': self.tc_asn_false_positive_last_reported_string,
            '#App:9876:tc.asn.id!String': self.tc_asn_id_string,
            '#App:9876:tc.asn.json.raw!String': self.tc_asn_json_raw_string,
            '#App:9876:tc.asn.last_modified!String': self.tc_asn_last_modified_string,
            '#App:9876:tc.asn.last_observed!String': self.tc_asn_last_observed_string,
            '#App:9876:tc.asn.observation_count!String': self.tc_asn_observation_count_string,
            '#App:9876:tc.asn.owner_name!String': self.tc_asn_owner_name_string,
            '#App:9876:tc.asn.private_flag!String': self.tc_asn_private_flag_string,
            '#App:9876:tc.asn.rating!String': self.tc_asn_rating_string,
            '#App:9876:tc.asn.security_label!StringArray': self.tc_asn_security_label_stringarray,
            '#App:9876:tc.asn.source!String': self.tc_asn_source_string,
            '#App:9876:tc.asn.success.count!String': self.tc_asn_success_count_string,
            '#App:9876:tc.asn.tag!StringArray': self.tc_asn_tag_stringarray,
            '#App:9876:tc.asn!TCEntity': self.tc_asn_tcentity,
            '#App:9876:tc.asn.threat_assess_score!String': self.tc_asn_threat_assess_score_string,
            '#App:9876:tc.asn.web_link!String': self.tc_asn_web_link_string,
            '#App:9876:tc.asns.action!String': self.tc_asns_action_string,
            '#App:9876:tc.asns.active_locked!StringArray': self.tc_asns_active_locked_stringarray,
            '#App:9876:tc.asns.active!StringArray': self.tc_asns_active_stringarray,
            '#App:9876:tc.asns.as_number!StringArray': self.tc_asns_as_number_stringarray,
            '#App:9876:tc.asns.confidence!StringArray': self.tc_asns_confidence_stringarray,
            '#App:9876:tc.asns.count!String': self.tc_asns_count_string,
            '#App:9876:tc.asns.date_added!StringArray': self.tc_asns_date_added_stringarray,
            '#App:9876:tc.asns.fail.count!String': self.tc_asns_fail_count_string,
            '#App:9876:tc.asns.false_positive_count!StringArray': self.tc_asns_false_positive_count_stringarray,
            '#App:9876:tc.asns.false_positive_last_reported!StringArray': self.tc_asns_false_positive_last_reported_stringarray,
            '#App:9876:tc.asns.id!StringArray': self.tc_asns_id_stringarray,
            '#App:9876:tc.asns.json.raw!String': self.tc_asns_json_raw_string,
            '#App:9876:tc.asns.last_modified!StringArray': self.tc_asns_last_modified_stringarray,
            '#App:9876:tc.asns.last_observed!StringArray': self.tc_asns_last_observed_stringarray,
            '#App:9876:tc.asns.observation_count!StringArray': self.tc_asns_observation_count_stringarray,
            '#App:9876:tc.asns.owner_name!StringArray': self.tc_asns_owner_name_stringarray,
            '#App:9876:tc.asns.private_flag!StringArray': self.tc_asns_private_flag_stringarray,
            '#App:9876:tc.asns.rating!StringArray': self.tc_asns_rating_stringarray,
            '#App:9876:tc.asns.source!StringArray': self.tc_asns_source_stringarray,
            '#App:9876:tc.asns.success.count!String': self.tc_asns_success_count_string,
            '#App:9876:tc.asns!TCEntityArray': self.tc_asns_tcentityarray,
            '#App:9876:tc.asns.threat_assess_score!StringArray': self.tc_asns_threat_assess_score_stringarray,
            '#App:9876:tc.asns.web_link!StringArray': self.tc_asns_web_link_stringarray,
            '#App:9876:tc.cidr_block.action!String': self.tc_cidr_block_action_string,
            '#App:9876:tc.cidr_block.active_locked!String': self.tc_cidr_block_active_locked_string,
            '#App:9876:tc.cidr_block.active!String': self.tc_cidr_block_active_string,
            '#App:9876:tc.cidr_block.attribute!TCEntityArray': self.tc_cidr_block_attribute_tcentityarray,
            '#App:9876:tc.cidr_block.block!String': self.tc_cidr_block_block_string,
            '#App:9876:tc.cidr_block.confidence!String': self.tc_cidr_block_confidence_string,
            '#App:9876:tc.cidr_block.count!String': self.tc_cidr_block_count_string,
            '#App:9876:tc.cidr_block.date_added!String': self.tc_cidr_block_date_added_string,
            '#App:9876:tc.cidr_block.fail.count!String': self.tc_cidr_block_fail_count_string,
            '#App:9876:tc.cidr_block.false_positive_count!String': self.tc_cidr_block_false_positive_count_string,
            '#App:9876:tc.cidr_block.false_positive_last_reported!String': self.tc_cidr_block_false_positive_last_reported_string,
            '#App:9876:tc.cidr_block.id!String': self.tc_cidr_block_id_string,
            '#App:9876:tc.cidr_block.json.raw!String': self.tc_cidr_block_json_raw_string,
            '#App:9876:tc.cidr_block.last_modified!String': self.tc_cidr_block_last_modified_string,
            '#App:9876:tc.cidr_block.last_observed!String': self.tc_cidr_block_last_observed_string,
            '#App:9876:tc.cidr_block.observation_count!String': self.tc_cidr_block_observation_count_string,
            '#App:9876:tc.cidr_block.owner_name!String': self.tc_cidr_block_owner_name_string,
            '#App:9876:tc.cidr_block.private_flag!String': self.tc_cidr_block_private_flag_string,
            '#App:9876:tc.cidr_block.rating!String': self.tc_cidr_block_rating_string,
            '#App:9876:tc.cidr_block.security_label!StringArray': self.tc_cidr_block_security_label_stringarray,
            '#App:9876:tc.cidr_block.source!String': self.tc_cidr_block_source_string,
            '#App:9876:tc.cidr_block.success.count!String': self.tc_cidr_block_success_count_string,
            '#App:9876:tc.cidr_block.tag!StringArray': self.tc_cidr_block_tag_stringarray,
            '#App:9876:tc.cidr_block!TCEntity': self.tc_cidr_block_tcentity,
            '#App:9876:tc.cidr_block.threat_assess_score!String': self.tc_cidr_block_threat_assess_score_string,
            '#App:9876:tc.cidr_block.web_link!String': self.tc_cidr_block_web_link_string,
            '#App:9876:tc.cidr_blocks.action!String': self.tc_cidr_blocks_action_string,
            '#App:9876:tc.cidr_blocks.active_locked!StringArray': self.tc_cidr_blocks_active_locked_stringarray,
            '#App:9876:tc.cidr_blocks.active!StringArray': self.tc_cidr_blocks_active_stringarray,
            '#App:9876:tc.cidr_blocks.block!StringArray': self.tc_cidr_blocks_block_stringarray,
            '#App:9876:tc.cidr_blocks.confidence!StringArray': self.tc_cidr_blocks_confidence_stringarray,
            '#App:9876:tc.cidr_blocks.count!String': self.tc_cidr_blocks_count_string,
            '#App:9876:tc.cidr_blocks.date_added!StringArray': self.tc_cidr_blocks_date_added_stringarray,
            '#App:9876:tc.cidr_blocks.fail.count!String': self.tc_cidr_blocks_fail_count_string,
            '#App:9876:tc.cidr_blocks.false_positive_count!StringArray': self.tc_cidr_blocks_false_positive_count_stringarray,
            '#App:9876:tc.cidr_blocks.false_positive_last_reported!StringArray': self.tc_cidr_blocks_false_positive_last_reported_stringarray,
            '#App:9876:tc.cidr_blocks.id!StringArray': self.tc_cidr_blocks_id_stringarray,
            '#App:9876:tc.cidr_blocks.json.raw!String': self.tc_cidr_blocks_json_raw_string,
            '#App:9876:tc.cidr_blocks.last_modified!StringArray': self.tc_cidr_blocks_last_modified_stringarray,
            '#App:9876:tc.cidr_blocks.last_observed!StringArray': self.tc_cidr_blocks_last_observed_stringarray,
            '#App:9876:tc.cidr_blocks.observation_count!StringArray': self.tc_cidr_blocks_observation_count_stringarray,
            '#App:9876:tc.cidr_blocks.owner_name!StringArray': self.tc_cidr_blocks_owner_name_stringarray,
            '#App:9876:tc.cidr_blocks.private_flag!StringArray': self.tc_cidr_blocks_private_flag_stringarray,
            '#App:9876:tc.cidr_blocks.rating!StringArray': self.tc_cidr_blocks_rating_stringarray,
            '#App:9876:tc.cidr_blocks.source!StringArray': self.tc_cidr_blocks_source_stringarray,
            '#App:9876:tc.cidr_blocks.success.count!String': self.tc_cidr_blocks_success_count_string,
            '#App:9876:tc.cidr_blocks!TCEntityArray': self.tc_cidr_blocks_tcentityarray,
            '#App:9876:tc.cidr_blocks.threat_assess_score!StringArray': self.tc_cidr_blocks_threat_assess_score_stringarray,
            '#App:9876:tc.cidr_blocks.web_link!StringArray': self.tc_cidr_blocks_web_link_stringarray,
            '#App:9876:tc.custom_indicator.action!String': self.tc_custom_indicator_action_string,
            '#App:9876:tc.custom_indicator.active_locked!String': self.tc_custom_indicator_active_locked_string,
            '#App:9876:tc.custom_indicator.active!String': self.tc_custom_indicator_active_string,
            '#App:9876:tc.custom_indicator.attribute!TCEntityArray': self.tc_custom_indicator_attribute_tcentityarray,
            '#App:9876:tc.custom_indicator.confidence!String': self.tc_custom_indicator_confidence_string,
            '#App:9876:tc.custom_indicator.count!String': self.tc_custom_indicator_count_string,
            '#App:9876:tc.custom_indicator.date_added!String': self.tc_custom_indicator_date_added_string,
            '#App:9876:tc.custom_indicator.fail.count!String': self.tc_custom_indicator_fail_count_string,
            '#App:9876:tc.custom_indicator.false_positive_count!String': self.tc_custom_indicator_false_positive_count_string,
            '#App:9876:tc.custom_indicator.false_positive_last_reported!String': self.tc_custom_indicator_false_positive_last_reported_string,
            '#App:9876:tc.custom_indicator.id!String': self.tc_custom_indicator_id_string,
            '#App:9876:tc.custom_indicator.json.raw!String': self.tc_custom_indicator_json_raw_string,
            '#App:9876:tc.custom_indicator.last_modified!String': self.tc_custom_indicator_last_modified_string,
            '#App:9876:tc.custom_indicator.last_observed!String': self.tc_custom_indicator_last_observed_string,
            '#App:9876:tc.custom_indicator.observation_count!String': self.tc_custom_indicator_observation_count_string,
            '#App:9876:tc.custom_indicator.owner_name!String': self.tc_custom_indicator_owner_name_string,
            '#App:9876:tc.custom_indicator.private_flag!String': self.tc_custom_indicator_private_flag_string,
            '#App:9876:tc.custom_indicator.rating!String': self.tc_custom_indicator_rating_string,
            '#App:9876:tc.custom_indicator.security_label!StringArray': self.tc_custom_indicator_security_label_stringarray,
            '#App:9876:tc.custom_indicator.source!String': self.tc_custom_indicator_source_string,
            '#App:9876:tc.custom_indicator.success.count!String': self.tc_custom_indicator_success_count_string,
            '#App:9876:tc.custom_indicator.tag!StringArray': self.tc_custom_indicator_tag_stringarray,
            '#App:9876:tc.custom_indicator!TCEntity': self.tc_custom_indicator_tcentity,
            '#App:9876:tc.custom_indicator.threat_assess_score!String': self.tc_custom_indicator_threat_assess_score_string,
            '#App:9876:tc.custom_indicator.value_1!String': self.tc_custom_indicator_value_1_string,
            '#App:9876:tc.custom_indicator.value_2!String': self.tc_custom_indicator_value_2_string,
            '#App:9876:tc.custom_indicator.value_3!String': self.tc_custom_indicator_value_3_string,
            '#App:9876:tc.custom_indicator.web_link!String': self.tc_custom_indicator_web_link_string,
            '#App:9876:tc.custom_indicators.action!String': self.tc_custom_indicators_action_string,
            '#App:9876:tc.custom_indicators.active_locked!StringArray': self.tc_custom_indicators_active_locked_stringarray,
            '#App:9876:tc.custom_indicators.active!StringArray': self.tc_custom_indicators_active_stringarray,
            '#App:9876:tc.custom_indicators.confidence!StringArray': self.tc_custom_indicators_confidence_stringarray,
            '#App:9876:tc.custom_indicators.count!String': self.tc_custom_indicators_count_string,
            '#App:9876:tc.custom_indicators.date_added!StringArray': self.tc_custom_indicators_date_added_stringarray,
            '#App:9876:tc.custom_indicators.fail.count!String': self.tc_custom_indicators_fail_count_string,
            '#App:9876:tc.custom_indicators.false_positive_count!StringArray': self.tc_custom_indicators_false_positive_count_stringarray,
            '#App:9876:tc.custom_indicators.false_positive_last_reported!StringArray': self.tc_custom_indicators_false_positive_last_reported_stringarray,
            '#App:9876:tc.custom_indicators.id!StringArray': self.tc_custom_indicators_id_stringarray,
            '#App:9876:tc.custom_indicators.json.raw!String': self.tc_custom_indicators_json_raw_string,
            '#App:9876:tc.custom_indicators.last_modified!StringArray': self.tc_custom_indicators_last_modified_stringarray,
            '#App:9876:tc.custom_indicators.last_observed!StringArray': self.tc_custom_indicators_last_observed_stringarray,
            '#App:9876:tc.custom_indicators.observation_count!StringArray': self.tc_custom_indicators_observation_count_stringarray,
            '#App:9876:tc.custom_indicators.owner_name!StringArray': self.tc_custom_indicators_owner_name_stringarray,
            '#App:9876:tc.custom_indicators.private_flag!StringArray': self.tc_custom_indicators_private_flag_stringarray,
            '#App:9876:tc.custom_indicators.rating!StringArray': self.tc_custom_indicators_rating_stringarray,
            '#App:9876:tc.custom_indicators.source!StringArray': self.tc_custom_indicators_source_stringarray,
            '#App:9876:tc.custom_indicators.success.count!String': self.tc_custom_indicators_success_count_string,
            '#App:9876:tc.custom_indicators!TCEntityArray': self.tc_custom_indicators_tcentityarray,
            '#App:9876:tc.custom_indicators.threat_assess_score!StringArray': self.tc_custom_indicators_threat_assess_score_stringarray,
            '#App:9876:tc.custom_indicators.value_1!StringArray': self.tc_custom_indicators_value_1_stringarray,
            '#App:9876:tc.custom_indicators.value_2!StringArray': self.tc_custom_indicators_value_2_stringarray,
            '#App:9876:tc.custom_indicators.value_3!StringArray': self.tc_custom_indicators_value_3_stringarray,
            '#App:9876:tc.custom_indicators.web_link!StringArray': self.tc_custom_indicators_web_link_stringarray,
            '#App:9876:tc.email_address.action!String': self.tc_email_address_action_string,
            '#App:9876:tc.email_address.active_locked!String': self.tc_email_address_active_locked_string,
            '#App:9876:tc.email_address.active!String': self.tc_email_address_active_string,
            '#App:9876:tc.email_address.address!String': self.tc_email_address_address_string,
            '#App:9876:tc.email_address.attribute!TCEntityArray': self.tc_email_address_attribute_tcentityarray,
            '#App:9876:tc.email_address.confidence!String': self.tc_email_address_confidence_string,
            '#App:9876:tc.email_address.count!String': self.tc_email_address_count_string,
            '#App:9876:tc.email_address.date_added!String': self.tc_email_address_date_added_string,
            '#App:9876:tc.email_address.fail.count!String': self.tc_email_address_fail_count_string,
            '#App:9876:tc.email_address.false_positive_count!String': self.tc_email_address_false_positive_count_string,
            '#App:9876:tc.email_address.false_positive_last_reported!String': self.tc_email_address_false_positive_last_reported_string,
            '#App:9876:tc.email_address.id!String': self.tc_email_address_id_string,
            '#App:9876:tc.email_address.json.raw!String': self.tc_email_address_json_raw_string,
            '#App:9876:tc.email_address.last_modified!String': self.tc_email_address_last_modified_string,
            '#App:9876:tc.email_address.last_observed!String': self.tc_email_address_last_observed_string,
            '#App:9876:tc.email_address.observation_count!String': self.tc_email_address_observation_count_string,
            '#App:9876:tc.email_address.owner_name!String': self.tc_email_address_owner_name_string,
            '#App:9876:tc.email_address.private_flag!String': self.tc_email_address_private_flag_string,
            '#App:9876:tc.email_address.rating!String': self.tc_email_address_rating_string,
            '#App:9876:tc.email_address.security_label!StringArray': self.tc_email_address_security_label_stringarray,
            '#App:9876:tc.email_address.source!String': self.tc_email_address_source_string,
            '#App:9876:tc.email_address.success.count!String': self.tc_email_address_success_count_string,
            '#App:9876:tc.email_address.tag!StringArray': self.tc_email_address_tag_stringarray,
            '#App:9876:tc.email_address!TCEntity': self.tc_email_address_tcentity,
            '#App:9876:tc.email_address.threat_assess_score!String': self.tc_email_address_threat_assess_score_string,
            '#App:9876:tc.email_address.web_link!String': self.tc_email_address_web_link_string,
            '#App:9876:tc.email_addresses.action!String': self.tc_email_addresses_action_string,
            '#App:9876:tc.email_addresses.active_locked!StringArray': self.tc_email_addresses_active_locked_stringarray,
            '#App:9876:tc.email_addresses.active!StringArray': self.tc_email_addresses_active_stringarray,
            '#App:9876:tc.email_addresses.address!StringArray': self.tc_email_addresses_address_stringarray,
            '#App:9876:tc.email_addresses.confidence!StringArray': self.tc_email_addresses_confidence_stringarray,
            '#App:9876:tc.email_addresses.count!String': self.tc_email_addresses_count_string,
            '#App:9876:tc.email_addresses.date_added!StringArray': self.tc_email_addresses_date_added_stringarray,
            '#App:9876:tc.email_addresses.fail.count!String': self.tc_email_addresses_fail_count_string,
            '#App:9876:tc.email_addresses.false_positive_count!StringArray': self.tc_email_addresses_false_positive_count_stringarray,
            '#App:9876:tc.email_addresses.false_positive_last_reported!StringArray': self.tc_email_addresses_false_positive_last_reported_stringarray,
            '#App:9876:tc.email_addresses.id!StringArray': self.tc_email_addresses_id_stringarray,
            '#App:9876:tc.email_addresses.json.raw!String': self.tc_email_addresses_json_raw_string,
            '#App:9876:tc.email_addresses.last_modified!StringArray': self.tc_email_addresses_last_modified_stringarray,
            '#App:9876:tc.email_addresses.last_observed!StringArray': self.tc_email_addresses_last_observed_stringarray,
            '#App:9876:tc.email_addresses.observation_count!StringArray': self.tc_email_addresses_observation_count_stringarray,
            '#App:9876:tc.email_addresses.owner_name!StringArray': self.tc_email_addresses_owner_name_stringarray,
            '#App:9876:tc.email_addresses.private_flag!StringArray': self.tc_email_addresses_private_flag_stringarray,
            '#App:9876:tc.email_addresses.rating!StringArray': self.tc_email_addresses_rating_stringarray,
            '#App:9876:tc.email_addresses.source!StringArray': self.tc_email_addresses_source_stringarray,
            '#App:9876:tc.email_addresses.success.count!String': self.tc_email_addresses_success_count_string,
            '#App:9876:tc.email_addresses!TCEntityArray': self.tc_email_addresses_tcentityarray,
            '#App:9876:tc.email_addresses.threat_assess_score!StringArray': self.tc_email_addresses_threat_assess_score_stringarray,
            '#App:9876:tc.email_addresses.web_link!StringArray': self.tc_email_addresses_web_link_stringarray,
            '#App:9876:tc.email_subject.action!String': self.tc_email_subject_action_string,
            '#App:9876:tc.email_subject.active_locked!String': self.tc_email_subject_active_locked_string,
            '#App:9876:tc.email_subject.active!String': self.tc_email_subject_active_string,
            '#App:9876:tc.email_subject.attribute!TCEntityArray': self.tc_email_subject_attribute_tcentityarray,
            '#App:9876:tc.email_subject.confidence!String': self.tc_email_subject_confidence_string,
            '#App:9876:tc.email_subject.count!String': self.tc_email_subject_count_string,
            '#App:9876:tc.email_subject.date_added!String': self.tc_email_subject_date_added_string,
            '#App:9876:tc.email_subject.fail.count!String': self.tc_email_subject_fail_count_string,
            '#App:9876:tc.email_subject.false_positive_count!String': self.tc_email_subject_false_positive_count_string,
            '#App:9876:tc.email_subject.false_positive_last_reported!String': self.tc_email_subject_false_positive_last_reported_string,
            '#App:9876:tc.email_subject.id!String': self.tc_email_subject_id_string,
            '#App:9876:tc.email_subject.json.raw!String': self.tc_email_subject_json_raw_string,
            '#App:9876:tc.email_subject.last_modified!String': self.tc_email_subject_last_modified_string,
            '#App:9876:tc.email_subject.last_observed!String': self.tc_email_subject_last_observed_string,
            '#App:9876:tc.email_subject.observation_count!String': self.tc_email_subject_observation_count_string,
            '#App:9876:tc.email_subject.owner_name!String': self.tc_email_subject_owner_name_string,
            '#App:9876:tc.email_subject.private_flag!String': self.tc_email_subject_private_flag_string,
            '#App:9876:tc.email_subject.rating!String': self.tc_email_subject_rating_string,
            '#App:9876:tc.email_subject.security_label!StringArray': self.tc_email_subject_security_label_stringarray,
            '#App:9876:tc.email_subject.source!String': self.tc_email_subject_source_string,
            '#App:9876:tc.email_subject.subject!String': self.tc_email_subject_subject_string,
            '#App:9876:tc.email_subject.success.count!String': self.tc_email_subject_success_count_string,
            '#App:9876:tc.email_subject.tag!StringArray': self.tc_email_subject_tag_stringarray,
            '#App:9876:tc.email_subject!TCEntity': self.tc_email_subject_tcentity,
            '#App:9876:tc.email_subject.threat_assess_score!String': self.tc_email_subject_threat_assess_score_string,
            '#App:9876:tc.email_subject.web_link!String': self.tc_email_subject_web_link_string,
            '#App:9876:tc.email_subjects.action!String': self.tc_email_subjects_action_string,
            '#App:9876:tc.email_subjects.active_locked!StringArray': self.tc_email_subjects_active_locked_stringarray,
            '#App:9876:tc.email_subjects.active!StringArray': self.tc_email_subjects_active_stringarray,
            '#App:9876:tc.email_subjects.confidence!StringArray': self.tc_email_subjects_confidence_stringarray,
            '#App:9876:tc.email_subjects.count!String': self.tc_email_subjects_count_string,
            '#App:9876:tc.email_subjects.date_added!StringArray': self.tc_email_subjects_date_added_stringarray,
            '#App:9876:tc.email_subjects.fail.count!String': self.tc_email_subjects_fail_count_string,
            '#App:9876:tc.email_subjects.false_positive_count!StringArray': self.tc_email_subjects_false_positive_count_stringarray,
            '#App:9876:tc.email_subjects.false_positive_last_reported!StringArray': self.tc_email_subjects_false_positive_last_reported_stringarray,
            '#App:9876:tc.email_subjects.id!StringArray': self.tc_email_subjects_id_stringarray,
            '#App:9876:tc.email_subjects.json.raw!String': self.tc_email_subjects_json_raw_string,
            '#App:9876:tc.email_subjects.last_modified!StringArray': self.tc_email_subjects_last_modified_stringarray,
            '#App:9876:tc.email_subjects.last_observed!StringArray': self.tc_email_subjects_last_observed_stringarray,
            '#App:9876:tc.email_subjects.observation_count!StringArray': self.tc_email_subjects_observation_count_stringarray,
            '#App:9876:tc.email_subjects.owner_name!StringArray': self.tc_email_subjects_owner_name_stringarray,
            '#App:9876:tc.email_subjects.private_flag!StringArray': self.tc_email_subjects_private_flag_stringarray,
            '#App:9876:tc.email_subjects.rating!StringArray': self.tc_email_subjects_rating_stringarray,
            '#App:9876:tc.email_subjects.source!StringArray': self.tc_email_subjects_source_stringarray,
            '#App:9876:tc.email_subjects.subject!StringArray': self.tc_email_subjects_subject_stringarray,
            '#App:9876:tc.email_subjects.success.count!String': self.tc_email_subjects_success_count_string,
            '#App:9876:tc.email_subjects!TCEntityArray': self.tc_email_subjects_tcentityarray,
            '#App:9876:tc.email_subjects.threat_assess_score!StringArray': self.tc_email_subjects_threat_assess_score_stringarray,
            '#App:9876:tc.email_subjects.web_link!StringArray': self.tc_email_subjects_web_link_stringarray,
            '#App:9876:tc.file.action!String': self.tc_file_action_string,
            '#App:9876:tc.file.active_locked!String': self.tc_file_active_locked_string,
            '#App:9876:tc.file.active!String': self.tc_file_active_string,
            '#App:9876:tc.file.attribute!TCEntityArray': self.tc_file_attribute_tcentityarray,
            '#App:9876:tc.file.confidence!String': self.tc_file_confidence_string,
            '#App:9876:tc.file.count!String': self.tc_file_count_string,
            '#App:9876:tc.file.date_added!String': self.tc_file_date_added_string,
            '#App:9876:tc.file.fail.count!String': self.tc_file_fail_count_string,
            '#App:9876:tc.file.false_positive_count!String': self.tc_file_false_positive_count_string,
            '#App:9876:tc.file.false_positive_last_reported!String': self.tc_file_false_positive_last_reported_string,
            '#App:9876:tc.file.id!String': self.tc_file_id_string,
            '#App:9876:tc.file.json.raw!String': self.tc_file_json_raw_string,
            '#App:9876:tc.file.last_modified!String': self.tc_file_last_modified_string,
            '#App:9876:tc.file.last_observed!String': self.tc_file_last_observed_string,
            '#App:9876:tc.file.md5!String': self.tc_file_md5_string,
            '#App:9876:tc.file.observation_count!String': self.tc_file_observation_count_string,
            '#App:9876:tc.file.owner_name!String': self.tc_file_owner_name_string,
            '#App:9876:tc.file.private_flag!String': self.tc_file_private_flag_string,
            '#App:9876:tc.file.rating!String': self.tc_file_rating_string,
            '#App:9876:tc.file.security_label!StringArray': self.tc_file_security_label_stringarray,
            '#App:9876:tc.file.sha1!String': self.tc_file_sha1_string,
            '#App:9876:tc.file.sha256!String': self.tc_file_sha256_string,
            '#App:9876:tc.file.size!String': self.tc_file_size_string,
            '#App:9876:tc.file.source!String': self.tc_file_source_string,
            '#App:9876:tc.file.success.count!String': self.tc_file_success_count_string,
            '#App:9876:tc.file.tag!StringArray': self.tc_file_tag_stringarray,
            '#App:9876:tc.file!TCEntity': self.tc_file_tcentity,
            '#App:9876:tc.file.threat_assess_score!String': self.tc_file_threat_assess_score_string,
            '#App:9876:tc.file.web_link!String': self.tc_file_web_link_string,
            '#App:9876:tc.files.action!String': self.tc_files_action_string,
            '#App:9876:tc.files.active_locked!StringArray': self.tc_files_active_locked_stringarray,
            '#App:9876:tc.files.active!StringArray': self.tc_files_active_stringarray,
            '#App:9876:tc.files.confidence!StringArray': self.tc_files_confidence_stringarray,
            '#App:9876:tc.files.count!String': self.tc_files_count_string,
            '#App:9876:tc.files.date_added!StringArray': self.tc_files_date_added_stringarray,
            '#App:9876:tc.files.fail.count!String': self.tc_files_fail_count_string,
            '#App:9876:tc.files.false_positive_count!StringArray': self.tc_files_false_positive_count_stringarray,
            '#App:9876:tc.files.false_positive_last_reported!StringArray': self.tc_files_false_positive_last_reported_stringarray,
            '#App:9876:tc.files.id!StringArray': self.tc_files_id_stringarray,
            '#App:9876:tc.files.json.raw!String': self.tc_files_json_raw_string,
            '#App:9876:tc.files.last_modified!StringArray': self.tc_files_last_modified_stringarray,
            '#App:9876:tc.files.last_observed!StringArray': self.tc_files_last_observed_stringarray,
            '#App:9876:tc.files.md5!StringArray': self.tc_files_md5_stringarray,
            '#App:9876:tc.files.observation_count!StringArray': self.tc_files_observation_count_stringarray,
            '#App:9876:tc.files.owner_name!StringArray': self.tc_files_owner_name_stringarray,
            '#App:9876:tc.files.private_flag!StringArray': self.tc_files_private_flag_stringarray,
            '#App:9876:tc.files.rating!StringArray': self.tc_files_rating_stringarray,
            '#App:9876:tc.files.sha1!StringArray': self.tc_files_sha1_stringarray,
            '#App:9876:tc.files.sha256!StringArray': self.tc_files_sha256_stringarray,
            '#App:9876:tc.files.size!StringArray': self.tc_files_size_stringarray,
            '#App:9876:tc.files.source!StringArray': self.tc_files_source_stringarray,
            '#App:9876:tc.files.success.count!String': self.tc_files_success_count_string,
            '#App:9876:tc.files!TCEntityArray': self.tc_files_tcentityarray,
            '#App:9876:tc.files.threat_assess_score!StringArray': self.tc_files_threat_assess_score_stringarray,
            '#App:9876:tc.files.web_link!StringArray': self.tc_files_web_link_stringarray,
            '#App:9876:tc.hashtag.action!String': self.tc_hashtag_action_string,
            '#App:9876:tc.hashtag.active_locked!String': self.tc_hashtag_active_locked_string,
            '#App:9876:tc.hashtag.active!String': self.tc_hashtag_active_string,
            '#App:9876:tc.hashtag.attribute!TCEntityArray': self.tc_hashtag_attribute_tcentityarray,
            '#App:9876:tc.hashtag.confidence!String': self.tc_hashtag_confidence_string,
            '#App:9876:tc.hashtag.count!String': self.tc_hashtag_count_string,
            '#App:9876:tc.hashtag.date_added!String': self.tc_hashtag_date_added_string,
            '#App:9876:tc.hashtag.fail.count!String': self.tc_hashtag_fail_count_string,
            '#App:9876:tc.hashtag.false_positive_count!String': self.tc_hashtag_false_positive_count_string,
            '#App:9876:tc.hashtag.false_positive_last_reported!String': self.tc_hashtag_false_positive_last_reported_string,
            '#App:9876:tc.hashtag.hashtag!String': self.tc_hashtag_hashtag_string,
            '#App:9876:tc.hashtag.id!String': self.tc_hashtag_id_string,
            '#App:9876:tc.hashtag.json.raw!String': self.tc_hashtag_json_raw_string,
            '#App:9876:tc.hashtag.last_modified!String': self.tc_hashtag_last_modified_string,
            '#App:9876:tc.hashtag.last_observed!String': self.tc_hashtag_last_observed_string,
            '#App:9876:tc.hashtag.observation_count!String': self.tc_hashtag_observation_count_string,
            '#App:9876:tc.hashtag.owner_name!String': self.tc_hashtag_owner_name_string,
            '#App:9876:tc.hashtag.private_flag!String': self.tc_hashtag_private_flag_string,
            '#App:9876:tc.hashtag.rating!String': self.tc_hashtag_rating_string,
            '#App:9876:tc.hashtag.security_label!StringArray': self.tc_hashtag_security_label_stringarray,
            '#App:9876:tc.hashtag.source!String': self.tc_hashtag_source_string,
            '#App:9876:tc.hashtag.success.count!String': self.tc_hashtag_success_count_string,
            '#App:9876:tc.hashtag.tag!StringArray': self.tc_hashtag_tag_stringarray,
            '#App:9876:tc.hashtag!TCEntity': self.tc_hashtag_tcentity,
            '#App:9876:tc.hashtag.threat_assess_score!String': self.tc_hashtag_threat_assess_score_string,
            '#App:9876:tc.hashtag.web_link!String': self.tc_hashtag_web_link_string,
            '#App:9876:tc.hashtags.action!String': self.tc_hashtags_action_string,
            '#App:9876:tc.hashtags.active_locked!StringArray': self.tc_hashtags_active_locked_stringarray,
            '#App:9876:tc.hashtags.active!StringArray': self.tc_hashtags_active_stringarray,
            '#App:9876:tc.hashtags.confidence!StringArray': self.tc_hashtags_confidence_stringarray,
            '#App:9876:tc.hashtags.count!String': self.tc_hashtags_count_string,
            '#App:9876:tc.hashtags.date_added!StringArray': self.tc_hashtags_date_added_stringarray,
            '#App:9876:tc.hashtags.fail.count!String': self.tc_hashtags_fail_count_string,
            '#App:9876:tc.hashtags.false_positive_count!StringArray': self.tc_hashtags_false_positive_count_stringarray,
            '#App:9876:tc.hashtags.false_positive_last_reported!StringArray': self.tc_hashtags_false_positive_last_reported_stringarray,
            '#App:9876:tc.hashtags.hashtag!StringArray': self.tc_hashtags_hashtag_stringarray,
            '#App:9876:tc.hashtags.id!StringArray': self.tc_hashtags_id_stringarray,
            '#App:9876:tc.hashtags.json.raw!String': self.tc_hashtags_json_raw_string,
            '#App:9876:tc.hashtags.last_modified!StringArray': self.tc_hashtags_last_modified_stringarray,
            '#App:9876:tc.hashtags.last_observed!StringArray': self.tc_hashtags_last_observed_stringarray,
            '#App:9876:tc.hashtags.observation_count!StringArray': self.tc_hashtags_observation_count_stringarray,
            '#App:9876:tc.hashtags.owner_name!StringArray': self.tc_hashtags_owner_name_stringarray,
            '#App:9876:tc.hashtags.private_flag!StringArray': self.tc_hashtags_private_flag_stringarray,
            '#App:9876:tc.hashtags.rating!StringArray': self.tc_hashtags_rating_stringarray,
            '#App:9876:tc.hashtags.source!StringArray': self.tc_hashtags_source_stringarray,
            '#App:9876:tc.hashtags.success.count!String': self.tc_hashtags_success_count_string,
            '#App:9876:tc.hashtags!TCEntityArray': self.tc_hashtags_tcentityarray,
            '#App:9876:tc.hashtags.threat_assess_score!StringArray': self.tc_hashtags_threat_assess_score_stringarray,
            '#App:9876:tc.hashtags.web_link!StringArray': self.tc_hashtags_web_link_stringarray,
            '#App:9876:tc.host.action!String': self.tc_host_action_string,
            '#App:9876:tc.host.active_locked!String': self.tc_host_active_locked_string,
            '#App:9876:tc.host.active!String': self.tc_host_active_string,
            '#App:9876:tc.host.attribute!TCEntityArray': self.tc_host_attribute_tcentityarray,
            '#App:9876:tc.host.confidence!String': self.tc_host_confidence_string,
            '#App:9876:tc.host.count!String': self.tc_host_count_string,
            '#App:9876:tc.host.date_added!String': self.tc_host_date_added_string,
            '#App:9876:tc.host.dns_active!String': self.tc_host_dns_active_string,
            '#App:9876:tc.host.fail.count!String': self.tc_host_fail_count_string,
            '#App:9876:tc.host.false_positive_count!String': self.tc_host_false_positive_count_string,
            '#App:9876:tc.host.false_positive_last_reported!String': self.tc_host_false_positive_last_reported_string,
            '#App:9876:tc.host.host_name!String': self.tc_host_host_name_string,
            '#App:9876:tc.host.id!String': self.tc_host_id_string,
            '#App:9876:tc.host.json.raw!String': self.tc_host_json_raw_string,
            '#App:9876:tc.host.last_modified!String': self.tc_host_last_modified_string,
            '#App:9876:tc.host.last_observed!String': self.tc_host_last_observed_string,
            '#App:9876:tc.host.observation_count!String': self.tc_host_observation_count_string,
            '#App:9876:tc.host.owner_name!String': self.tc_host_owner_name_string,
            '#App:9876:tc.host.private_flag!String': self.tc_host_private_flag_string,
            '#App:9876:tc.host.rating!String': self.tc_host_rating_string,
            '#App:9876:tc.host.security_label!StringArray': self.tc_host_security_label_stringarray,
            '#App:9876:tc.host.source!String': self.tc_host_source_string,
            '#App:9876:tc.host.success.count!String': self.tc_host_success_count_string,
            '#App:9876:tc.host.tag!StringArray': self.tc_host_tag_stringarray,
            '#App:9876:tc.host!TCEntity': self.tc_host_tcentity,
            '#App:9876:tc.host.threat_assess_score!String': self.tc_host_threat_assess_score_string,
            '#App:9876:tc.host.web_link!String': self.tc_host_web_link_string,
            '#App:9876:tc.host.whois_active!String': self.tc_host_whois_active_string,
            '#App:9876:tc.hosts.action!String': self.tc_hosts_action_string,
            '#App:9876:tc.hosts.active_locked!StringArray': self.tc_hosts_active_locked_stringarray,
            '#App:9876:tc.hosts.active!StringArray': self.tc_hosts_active_stringarray,
            '#App:9876:tc.hosts.confidence!StringArray': self.tc_hosts_confidence_stringarray,
            '#App:9876:tc.hosts.count!String': self.tc_hosts_count_string,
            '#App:9876:tc.hosts.date_added!StringArray': self.tc_hosts_date_added_stringarray,
            '#App:9876:tc.hosts.dns_active!StringArray': self.tc_hosts_dns_active_stringarray,
            '#App:9876:tc.hosts.fail.count!String': self.tc_hosts_fail_count_string,
            '#App:9876:tc.hosts.false_positive_count!StringArray': self.tc_hosts_false_positive_count_stringarray,
            '#App:9876:tc.hosts.false_positive_last_reported!StringArray': self.tc_hosts_false_positive_last_reported_stringarray,
            '#App:9876:tc.hosts.host_name!StringArray': self.tc_hosts_host_name_stringarray,
            '#App:9876:tc.hosts.id!StringArray': self.tc_hosts_id_stringarray,
            '#App:9876:tc.hosts.json.raw!String': self.tc_hosts_json_raw_string,
            '#App:9876:tc.hosts.last_modified!StringArray': self.tc_hosts_last_modified_stringarray,
            '#App:9876:tc.hosts.last_observed!StringArray': self.tc_hosts_last_observed_stringarray,
            '#App:9876:tc.hosts.observation_count!StringArray': self.tc_hosts_observation_count_stringarray,
            '#App:9876:tc.hosts.owner_name!StringArray': self.tc_hosts_owner_name_stringarray,
            '#App:9876:tc.hosts.private_flag!StringArray': self.tc_hosts_private_flag_stringarray,
            '#App:9876:tc.hosts.rating!StringArray': self.tc_hosts_rating_stringarray,
            '#App:9876:tc.hosts.source!StringArray': self.tc_hosts_source_stringarray,
            '#App:9876:tc.hosts.success.count!String': self.tc_hosts_success_count_string,
            '#App:9876:tc.hosts!TCEntityArray': self.tc_hosts_tcentityarray,
            '#App:9876:tc.hosts.threat_assess_score!StringArray': self.tc_hosts_threat_assess_score_stringarray,
            '#App:9876:tc.hosts.web_link!StringArray': self.tc_hosts_web_link_stringarray,
            '#App:9876:tc.hosts.whois_active!StringArray': self.tc_hosts_whois_active_stringarray,
            '#App:9876:tc.indicator.action!String': self.tc_indicator_action_string,
            '#App:9876:tc.indicator.active_locked!String': self.tc_indicator_active_locked_string,
            '#App:9876:tc.indicator.active!String': self.tc_indicator_active_string,
            '#App:9876:tc.indicator.attribute!TCEntityArray': self.tc_indicator_attribute_tcentityarray,
            '#App:9876:tc.indicator.confidence!String': self.tc_indicator_confidence_string,
            '#App:9876:tc.indicator.count!String': self.tc_indicator_count_string,
            '#App:9876:tc.indicator.date_added!String': self.tc_indicator_date_added_string,
            '#App:9876:tc.indicator.fail.count!String': self.tc_indicator_fail_count_string,
            '#App:9876:tc.indicator.false_positive_count!String': self.tc_indicator_false_positive_count_string,
            '#App:9876:tc.indicator.false_positive_last_reported!String': self.tc_indicator_false_positive_last_reported_string,
            '#App:9876:tc.indicator.id!String': self.tc_indicator_id_string,
            '#App:9876:tc.indicator.json.raw!String': self.tc_indicator_json_raw_string,
            '#App:9876:tc.indicator.last_modified!String': self.tc_indicator_last_modified_string,
            '#App:9876:tc.indicator.last_observed!String': self.tc_indicator_last_observed_string,
            '#App:9876:tc.indicator.observation_count!String': self.tc_indicator_observation_count_string,
            '#App:9876:tc.indicator.owner_name!String': self.tc_indicator_owner_name_string,
            '#App:9876:tc.indicator.private_flag!String': self.tc_indicator_private_flag_string,
            '#App:9876:tc.indicator.rating!String': self.tc_indicator_rating_string,
            '#App:9876:tc.indicator.security_label!StringArray': self.tc_indicator_security_label_stringarray,
            '#App:9876:tc.indicator.source!String': self.tc_indicator_source_string,
            '#App:9876:tc.indicator.success.count!String': self.tc_indicator_success_count_string,
            '#App:9876:tc.indicator.summary!String': self.tc_indicator_summary_string,
            '#App:9876:tc.indicator.tag!StringArray': self.tc_indicator_tag_stringarray,
            '#App:9876:tc.indicator!TCEntity': self.tc_indicator_tcentity,
            '#App:9876:tc.indicator.threat_assess_score!String': self.tc_indicator_threat_assess_score_string,
            '#App:9876:tc.indicator.web_link!String': self.tc_indicator_web_link_string,
            '#App:9876:tc.indicators.action!String': self.tc_indicators_action_string,
            '#App:9876:tc.indicators.active_locked!StringArray': self.tc_indicators_active_locked_stringarray,
            '#App:9876:tc.indicators.active!StringArray': self.tc_indicators_active_stringarray,
            '#App:9876:tc.indicators.confidence!StringArray': self.tc_indicators_confidence_stringarray,
            '#App:9876:tc.indicators.count!String': self.tc_indicators_count_string,
            '#App:9876:tc.indicators.date_added!StringArray': self.tc_indicators_date_added_stringarray,
            '#App:9876:tc.indicators.fail.count!String': self.tc_indicators_fail_count_string,
            '#App:9876:tc.indicators.false_positive_count!StringArray': self.tc_indicators_false_positive_count_stringarray,
            '#App:9876:tc.indicators.false_positive_last_reported!StringArray': self.tc_indicators_false_positive_last_reported_stringarray,
            '#App:9876:tc.indicators.id!StringArray': self.tc_indicators_id_stringarray,
            '#App:9876:tc.indicators.json.raw!String': self.tc_indicators_json_raw_string,
            '#App:9876:tc.indicators.last_modified!StringArray': self.tc_indicators_last_modified_stringarray,
            '#App:9876:tc.indicators.last_observed!StringArray': self.tc_indicators_last_observed_stringarray,
            '#App:9876:tc.indicators.observation_count!StringArray': self.tc_indicators_observation_count_stringarray,
            '#App:9876:tc.indicators.owner_name!StringArray': self.tc_indicators_owner_name_stringarray,
            '#App:9876:tc.indicators.private_flag!StringArray': self.tc_indicators_private_flag_stringarray,
            '#App:9876:tc.indicators.rating!StringArray': self.tc_indicators_rating_stringarray,
            '#App:9876:tc.indicators.source!StringArray': self.tc_indicators_source_stringarray,
            '#App:9876:tc.indicators.success.count!String': self.tc_indicators_success_count_string,
            '#App:9876:tc.indicators.summary!StringArray': self.tc_indicators_summary_stringarray,
            '#App:9876:tc.indicators!TCEntityArray': self.tc_indicators_tcentityarray,
            '#App:9876:tc.indicators.threat_assess_score!StringArray': self.tc_indicators_threat_assess_score_stringarray,
            '#App:9876:tc.indicators.web_link!StringArray': self.tc_indicators_web_link_stringarray,
            '#App:9876:tc.mutex.action!String': self.tc_mutex_action_string,
            '#App:9876:tc.mutex.active_locked!String': self.tc_mutex_active_locked_string,
            '#App:9876:tc.mutex.active!String': self.tc_mutex_active_string,
            '#App:9876:tc.mutex.attribute!TCEntityArray': self.tc_mutex_attribute_tcentityarray,
            '#App:9876:tc.mutex.confidence!String': self.tc_mutex_confidence_string,
            '#App:9876:tc.mutex.count!String': self.tc_mutex_count_string,
            '#App:9876:tc.mutex.date_added!String': self.tc_mutex_date_added_string,
            '#App:9876:tc.mutex.fail.count!String': self.tc_mutex_fail_count_string,
            '#App:9876:tc.mutex.false_positive_count!String': self.tc_mutex_false_positive_count_string,
            '#App:9876:tc.mutex.false_positive_last_reported!String': self.tc_mutex_false_positive_last_reported_string,
            '#App:9876:tc.mutex.id!String': self.tc_mutex_id_string,
            '#App:9876:tc.mutex.json.raw!String': self.tc_mutex_json_raw_string,
            '#App:9876:tc.mutex.last_modified!String': self.tc_mutex_last_modified_string,
            '#App:9876:tc.mutex.last_observed!String': self.tc_mutex_last_observed_string,
            '#App:9876:tc.mutex.mutex!String': self.tc_mutex_mutex_string,
            '#App:9876:tc.mutex.observation_count!String': self.tc_mutex_observation_count_string,
            '#App:9876:tc.mutex.owner_name!String': self.tc_mutex_owner_name_string,
            '#App:9876:tc.mutex.private_flag!String': self.tc_mutex_private_flag_string,
            '#App:9876:tc.mutex.rating!String': self.tc_mutex_rating_string,
            '#App:9876:tc.mutex.security_label!StringArray': self.tc_mutex_security_label_stringarray,
            '#App:9876:tc.mutex.source!String': self.tc_mutex_source_string,
            '#App:9876:tc.mutex.success.count!String': self.tc_mutex_success_count_string,
            '#App:9876:tc.mutex.tag!StringArray': self.tc_mutex_tag_stringarray,
            '#App:9876:tc.mutex!TCEntity': self.tc_mutex_tcentity,
            '#App:9876:tc.mutex.threat_assess_score!String': self.tc_mutex_threat_assess_score_string,
            '#App:9876:tc.mutex.web_link!String': self.tc_mutex_web_link_string,
            '#App:9876:tc.mutexes.action!String': self.tc_mutexes_action_string,
            '#App:9876:tc.mutexes.active_locked!StringArray': self.tc_mutexes_active_locked_stringarray,
            '#App:9876:tc.mutexes.active!StringArray': self.tc_mutexes_active_stringarray,
            '#App:9876:tc.mutexes.confidence!StringArray': self.tc_mutexes_confidence_stringarray,
            '#App:9876:tc.mutexes.count!String': self.tc_mutexes_count_string,
            '#App:9876:tc.mutexes.date_added!StringArray': self.tc_mutexes_date_added_stringarray,
            '#App:9876:tc.mutexes.fail.count!String': self.tc_mutexes_fail_count_string,
            '#App:9876:tc.mutexes.false_positive_count!StringArray': self.tc_mutexes_false_positive_count_stringarray,
            '#App:9876:tc.mutexes.false_positive_last_reported!StringArray': self.tc_mutexes_false_positive_last_reported_stringarray,
            '#App:9876:tc.mutexes.id!StringArray': self.tc_mutexes_id_stringarray,
            '#App:9876:tc.mutexes.json.raw!String': self.tc_mutexes_json_raw_string,
            '#App:9876:tc.mutexes.last_modified!StringArray': self.tc_mutexes_last_modified_stringarray,
            '#App:9876:tc.mutexes.last_observed!StringArray': self.tc_mutexes_last_observed_stringarray,
            '#App:9876:tc.mutexes.mutex!StringArray': self.tc_mutexes_mutex_stringarray,
            '#App:9876:tc.mutexes.observation_count!StringArray': self.tc_mutexes_observation_count_stringarray,
            '#App:9876:tc.mutexes.owner_name!StringArray': self.tc_mutexes_owner_name_stringarray,
            '#App:9876:tc.mutexes.private_flag!StringArray': self.tc_mutexes_private_flag_stringarray,
            '#App:9876:tc.mutexes.rating!StringArray': self.tc_mutexes_rating_stringarray,
            '#App:9876:tc.mutexes.source!StringArray': self.tc_mutexes_source_stringarray,
            '#App:9876:tc.mutexes.success.count!String': self.tc_mutexes_success_count_string,
            '#App:9876:tc.mutexes!TCEntityArray': self.tc_mutexes_tcentityarray,
            '#App:9876:tc.mutexes.threat_assess_score!StringArray': self.tc_mutexes_threat_assess_score_stringarray,
            '#App:9876:tc.mutexes.web_link!StringArray': self.tc_mutexes_web_link_stringarray,
            '#App:9876:tc.registry_key.action!String': self.tc_registry_key_action_string,
            '#App:9876:tc.registry_key.active_locked!String': self.tc_registry_key_active_locked_string,
            '#App:9876:tc.registry_key.active!String': self.tc_registry_key_active_string,
            '#App:9876:tc.registry_key.attribute!TCEntityArray': self.tc_registry_key_attribute_tcentityarray,
            '#App:9876:tc.registry_key.confidence!String': self.tc_registry_key_confidence_string,
            '#App:9876:tc.registry_key.count!String': self.tc_registry_key_count_string,
            '#App:9876:tc.registry_key.date_added!String': self.tc_registry_key_date_added_string,
            '#App:9876:tc.registry_key.fail.count!String': self.tc_registry_key_fail_count_string,
            '#App:9876:tc.registry_key.false_positive_count!String': self.tc_registry_key_false_positive_count_string,
            '#App:9876:tc.registry_key.false_positive_last_reported!String': self.tc_registry_key_false_positive_last_reported_string,
            '#App:9876:tc.registry_key.id!String': self.tc_registry_key_id_string,
            '#App:9876:tc.registry_key.json.raw!String': self.tc_registry_key_json_raw_string,
            '#App:9876:tc.registry_key.key_name!String': self.tc_registry_key_key_name_string,
            '#App:9876:tc.registry_key.last_modified!String': self.tc_registry_key_last_modified_string,
            '#App:9876:tc.registry_key.last_observed!String': self.tc_registry_key_last_observed_string,
            '#App:9876:tc.registry_key.observation_count!String': self.tc_registry_key_observation_count_string,
            '#App:9876:tc.registry_key.owner_name!String': self.tc_registry_key_owner_name_string,
            '#App:9876:tc.registry_key.private_flag!String': self.tc_registry_key_private_flag_string,
            '#App:9876:tc.registry_key.rating!String': self.tc_registry_key_rating_string,
            '#App:9876:tc.registry_key.security_label!StringArray': self.tc_registry_key_security_label_stringarray,
            '#App:9876:tc.registry_key.source!String': self.tc_registry_key_source_string,
            '#App:9876:tc.registry_key.success.count!String': self.tc_registry_key_success_count_string,
            '#App:9876:tc.registry_key.tag!StringArray': self.tc_registry_key_tag_stringarray,
            '#App:9876:tc.registry_key!TCEntity': self.tc_registry_key_tcentity,
            '#App:9876:tc.registry_key.threat_assess_score!String': self.tc_registry_key_threat_assess_score_string,
            '#App:9876:tc.registry_key.value_name!String': self.tc_registry_key_value_name_string,
            '#App:9876:tc.registry_key.value_type!String': self.tc_registry_key_value_type_string,
            '#App:9876:tc.registry_key.web_link!String': self.tc_registry_key_web_link_string,
            '#App:9876:tc.registry_keys.action!String': self.tc_registry_keys_action_string,
            '#App:9876:tc.registry_keys.active_locked!StringArray': self.tc_registry_keys_active_locked_stringarray,
            '#App:9876:tc.registry_keys.active!StringArray': self.tc_registry_keys_active_stringarray,
            '#App:9876:tc.registry_keys.confidence!StringArray': self.tc_registry_keys_confidence_stringarray,
            '#App:9876:tc.registry_keys.count!String': self.tc_registry_keys_count_string,
            '#App:9876:tc.registry_keys.date_added!StringArray': self.tc_registry_keys_date_added_stringarray,
            '#App:9876:tc.registry_keys.fail.count!String': self.tc_registry_keys_fail_count_string,
            '#App:9876:tc.registry_keys.false_positive_count!StringArray': self.tc_registry_keys_false_positive_count_stringarray,
            '#App:9876:tc.registry_keys.false_positive_last_reported!StringArray': self.tc_registry_keys_false_positive_last_reported_stringarray,
            '#App:9876:tc.registry_keys.id!StringArray': self.tc_registry_keys_id_stringarray,
            '#App:9876:tc.registry_keys.json.raw!String': self.tc_registry_keys_json_raw_string,
            '#App:9876:tc.registry_keys.key_name!StringArray': self.tc_registry_keys_key_name_stringarray,
            '#App:9876:tc.registry_keys.last_modified!StringArray': self.tc_registry_keys_last_modified_stringarray,
            '#App:9876:tc.registry_keys.last_observed!StringArray': self.tc_registry_keys_last_observed_stringarray,
            '#App:9876:tc.registry_keys.observation_count!StringArray': self.tc_registry_keys_observation_count_stringarray,
            '#App:9876:tc.registry_keys.owner_name!StringArray': self.tc_registry_keys_owner_name_stringarray,
            '#App:9876:tc.registry_keys.private_flag!StringArray': self.tc_registry_keys_private_flag_stringarray,
            '#App:9876:tc.registry_keys.rating!StringArray': self.tc_registry_keys_rating_stringarray,
            '#App:9876:tc.registry_keys.source!StringArray': self.tc_registry_keys_source_stringarray,
            '#App:9876:tc.registry_keys.success.count!String': self.tc_registry_keys_success_count_string,
            '#App:9876:tc.registry_keys!TCEntityArray': self.tc_registry_keys_tcentityarray,
            '#App:9876:tc.registry_keys.threat_assess_score!StringArray': self.tc_registry_keys_threat_assess_score_stringarray,
            '#App:9876:tc.registry_keys.value_name!StringArray': self.tc_registry_keys_value_name_stringarray,
            '#App:9876:tc.registry_keys.value_type!StringArray': self.tc_registry_keys_value_type_stringarray,
            '#App:9876:tc.registry_keys.web_link!StringArray': self.tc_registry_keys_web_link_stringarray,
            '#App:9876:tc.url.action!String': self.tc_url_action_string,
            '#App:9876:tc.url.active_locked!String': self.tc_url_active_locked_string,
            '#App:9876:tc.url.active!String': self.tc_url_active_string,
            '#App:9876:tc.url.attribute!TCEntityArray': self.tc_url_attribute_tcentityarray,
            '#App:9876:tc.url.confidence!String': self.tc_url_confidence_string,
            '#App:9876:tc.url.count!String': self.tc_url_count_string,
            '#App:9876:tc.url.date_added!String': self.tc_url_date_added_string,
            '#App:9876:tc.url.fail.count!String': self.tc_url_fail_count_string,
            '#App:9876:tc.url.false_positive_count!String': self.tc_url_false_positive_count_string,
            '#App:9876:tc.url.false_positive_last_reported!String': self.tc_url_false_positive_last_reported_string,
            '#App:9876:tc.url.id!String': self.tc_url_id_string,
            '#App:9876:tc.url.json.raw!String': self.tc_url_json_raw_string,
            '#App:9876:tc.url.last_modified!String': self.tc_url_last_modified_string,
            '#App:9876:tc.url.last_observed!String': self.tc_url_last_observed_string,
            '#App:9876:tc.url.observation_count!String': self.tc_url_observation_count_string,
            '#App:9876:tc.url.owner_name!String': self.tc_url_owner_name_string,
            '#App:9876:tc.url.private_flag!String': self.tc_url_private_flag_string,
            '#App:9876:tc.url.rating!String': self.tc_url_rating_string,
            '#App:9876:tc.url.security_label!StringArray': self.tc_url_security_label_stringarray,
            '#App:9876:tc.url.source!String': self.tc_url_source_string,
            '#App:9876:tc.url.success.count!String': self.tc_url_success_count_string,
            '#App:9876:tc.url.tag!StringArray': self.tc_url_tag_stringarray,
            '#App:9876:tc.url!TCEntity': self.tc_url_tcentity,
            '#App:9876:tc.url.text!String': self.tc_url_text_string,
            '#App:9876:tc.url.threat_assess_score!String': self.tc_url_threat_assess_score_string,
            '#App:9876:tc.url.web_link!String': self.tc_url_web_link_string,
            '#App:9876:tc.urls.action!String': self.tc_urls_action_string,
            '#App:9876:tc.urls.active_locked!StringArray': self.tc_urls_active_locked_stringarray,
            '#App:9876:tc.urls.active!StringArray': self.tc_urls_active_stringarray,
            '#App:9876:tc.urls.confidence!StringArray': self.tc_urls_confidence_stringarray,
            '#App:9876:tc.urls.count!String': self.tc_urls_count_string,
            '#App:9876:tc.urls.date_added!StringArray': self.tc_urls_date_added_stringarray,
            '#App:9876:tc.urls.fail.count!String': self.tc_urls_fail_count_string,
            '#App:9876:tc.urls.false_positive_count!StringArray': self.tc_urls_false_positive_count_stringarray,
            '#App:9876:tc.urls.false_positive_last_reported!StringArray': self.tc_urls_false_positive_last_reported_stringarray,
            '#App:9876:tc.urls.id!StringArray': self.tc_urls_id_stringarray,
            '#App:9876:tc.urls.json.raw!String': self.tc_urls_json_raw_string,
            '#App:9876:tc.urls.last_modified!StringArray': self.tc_urls_last_modified_stringarray,
            '#App:9876:tc.urls.last_observed!StringArray': self.tc_urls_last_observed_stringarray,
            '#App:9876:tc.urls.observation_count!StringArray': self.tc_urls_observation_count_stringarray,
            '#App:9876:tc.urls.owner_name!StringArray': self.tc_urls_owner_name_stringarray,
            '#App:9876:tc.urls.private_flag!StringArray': self.tc_urls_private_flag_stringarray,
            '#App:9876:tc.urls.rating!StringArray': self.tc_urls_rating_stringarray,
            '#App:9876:tc.urls.source!StringArray': self.tc_urls_source_stringarray,
            '#App:9876:tc.urls.success.count!String': self.tc_urls_success_count_string,
            '#App:9876:tc.urls!TCEntityArray': self.tc_urls_tcentityarray,
            '#App:9876:tc.urls.text!StringArray': self.tc_urls_text_stringarray,
            '#App:9876:tc.urls.threat_assess_score!StringArray': self.tc_urls_threat_assess_score_stringarray,
            '#App:9876:tc.urls.web_link!StringArray': self.tc_urls_web_link_stringarray,
            '#App:9876:tc.user_agent.action!String': self.tc_user_agent_action_string,
            '#App:9876:tc.user_agent.active_locked!String': self.tc_user_agent_active_locked_string,
            '#App:9876:tc.user_agent.active!String': self.tc_user_agent_active_string,
            '#App:9876:tc.user_agent.attribute!TCEntityArray': self.tc_user_agent_attribute_tcentityarray,
            '#App:9876:tc.user_agent.confidence!String': self.tc_user_agent_confidence_string,
            '#App:9876:tc.user_agent.count!String': self.tc_user_agent_count_string,
            '#App:9876:tc.user_agent.date_added!String': self.tc_user_agent_date_added_string,
            '#App:9876:tc.user_agent.fail.count!String': self.tc_user_agent_fail_count_string,
            '#App:9876:tc.user_agent.false_positive_count!String': self.tc_user_agent_false_positive_count_string,
            '#App:9876:tc.user_agent.false_positive_last_reported!String': self.tc_user_agent_false_positive_last_reported_string,
            '#App:9876:tc.user_agent.id!String': self.tc_user_agent_id_string,
            '#App:9876:tc.user_agent.json.raw!String': self.tc_user_agent_json_raw_string,
            '#App:9876:tc.user_agent.last_modified!String': self.tc_user_agent_last_modified_string,
            '#App:9876:tc.user_agent.last_observed!String': self.tc_user_agent_last_observed_string,
            '#App:9876:tc.user_agent.observation_count!String': self.tc_user_agent_observation_count_string,
            '#App:9876:tc.user_agent.owner_name!String': self.tc_user_agent_owner_name_string,
            '#App:9876:tc.user_agent.private_flag!String': self.tc_user_agent_private_flag_string,
            '#App:9876:tc.user_agent.rating!String': self.tc_user_agent_rating_string,
            '#App:9876:tc.user_agent.security_label!StringArray': self.tc_user_agent_security_label_stringarray,
            '#App:9876:tc.user_agent.source!String': self.tc_user_agent_source_string,
            '#App:9876:tc.user_agent.success.count!String': self.tc_user_agent_success_count_string,
            '#App:9876:tc.user_agent.tag!StringArray': self.tc_user_agent_tag_stringarray,
            '#App:9876:tc.user_agent!TCEntity': self.tc_user_agent_tcentity,
            '#App:9876:tc.user_agent.threat_assess_score!String': self.tc_user_agent_threat_assess_score_string,
            '#App:9876:tc.user_agent.user_agent_string!String': self.tc_user_agent_user_agent_string_string,
            '#App:9876:tc.user_agent.web_link!String': self.tc_user_agent_web_link_string,
            '#App:9876:tc.user_agents.action!String': self.tc_user_agents_action_string,
            '#App:9876:tc.user_agents.active_locked!StringArray': self.tc_user_agents_active_locked_stringarray,
            '#App:9876:tc.user_agents.active!StringArray': self.tc_user_agents_active_stringarray,
            '#App:9876:tc.user_agents.confidence!StringArray': self.tc_user_agents_confidence_stringarray,
            '#App:9876:tc.user_agents.count!String': self.tc_user_agents_count_string,
            '#App:9876:tc.user_agents.date_added!StringArray': self.tc_user_agents_date_added_stringarray,
            '#App:9876:tc.user_agents.fail.count!String': self.tc_user_agents_fail_count_string,
            '#App:9876:tc.user_agents.false_positive_count!StringArray': self.tc_user_agents_false_positive_count_stringarray,
            '#App:9876:tc.user_agents.false_positive_last_reported!StringArray': self.tc_user_agents_false_positive_last_reported_stringarray,
            '#App:9876:tc.user_agents.id!StringArray': self.tc_user_agents_id_stringarray,
            '#App:9876:tc.user_agents.json.raw!String': self.tc_user_agents_json_raw_string,
            '#App:9876:tc.user_agents.last_modified!StringArray': self.tc_user_agents_last_modified_stringarray,
            '#App:9876:tc.user_agents.last_observed!StringArray': self.tc_user_agents_last_observed_stringarray,
            '#App:9876:tc.user_agents.observation_count!StringArray': self.tc_user_agents_observation_count_stringarray,
            '#App:9876:tc.user_agents.owner_name!StringArray': self.tc_user_agents_owner_name_stringarray,
            '#App:9876:tc.user_agents.private_flag!StringArray': self.tc_user_agents_private_flag_stringarray,
            '#App:9876:tc.user_agents.rating!StringArray': self.tc_user_agents_rating_stringarray,
            '#App:9876:tc.user_agents.source!StringArray': self.tc_user_agents_source_stringarray,
            '#App:9876:tc.user_agents.success.count!String': self.tc_user_agents_success_count_string,
            '#App:9876:tc.user_agents!TCEntityArray': self.tc_user_agents_tcentityarray,
            '#App:9876:tc.user_agents.threat_assess_score!StringArray': self.tc_user_agents_threat_assess_score_stringarray,
            '#App:9876:tc.user_agents.user_agent_string!StringArray': self.tc_user_agents_user_agent_string_stringarray,
            '#App:9876:tc.user_agents.web_link!StringArray': self.tc_user_agents_web_link_stringarray,
        }

    def validate(self, output_variables):
        """Validate Redis output data."""
        if output_variables is None:
            return

        for k, v in output_variables.items():
            method = self.validation_methods.get(k)
            if method is not None:
                method(dict(v))
            else:
                assert False, f'Unknown output variable found in profile: {k}'

    def tc_address_action_string(self, data):
        """Assert for #App:9876:tc.address.action!String."""
        output_var = '#App:9876:tc.address.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_active_locked_string(self, data):
        """Assert for #App:9876:tc.address.active_locked!String."""
        output_var = '#App:9876:tc.address.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_active_string(self, data):
        """Assert for #App:9876:tc.address.active!String."""
        output_var = '#App:9876:tc.address.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.address.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.address.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_confidence_string(self, data):
        """Assert for #App:9876:tc.address.confidence!String."""
        output_var = '#App:9876:tc.address.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_count_string(self, data):
        """Assert for #App:9876:tc.address.count!String."""
        output_var = '#App:9876:tc.address.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_date_added_string(self, data):
        """Assert for #App:9876:tc.address.date_added!String."""
        output_var = '#App:9876:tc.address.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_fail_count_string(self, data):
        """Assert for #App:9876:tc.address.fail.count!String."""
        output_var = '#App:9876:tc.address.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.address.false_positive_count!String."""
        output_var = '#App:9876:tc.address.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.address.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.address.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_id_string(self, data):
        """Assert for #App:9876:tc.address.id!String."""
        output_var = '#App:9876:tc.address.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_ip_string(self, data):
        """Assert for #App:9876:tc.address.ip!String."""
        output_var = '#App:9876:tc.address.ip!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_json_raw_string(self, data):
        """Assert for #App:9876:tc.address.json.raw!String."""
        output_var = '#App:9876:tc.address.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_last_modified_string(self, data):
        """Assert for #App:9876:tc.address.last_modified!String."""
        output_var = '#App:9876:tc.address.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_last_observed_string(self, data):
        """Assert for #App:9876:tc.address.last_observed!String."""
        output_var = '#App:9876:tc.address.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_observation_count_string(self, data):
        """Assert for #App:9876:tc.address.observation_count!String."""
        output_var = '#App:9876:tc.address.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_owner_name_string(self, data):
        """Assert for #App:9876:tc.address.owner_name!String."""
        output_var = '#App:9876:tc.address.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_private_flag_string(self, data):
        """Assert for #App:9876:tc.address.private_flag!String."""
        output_var = '#App:9876:tc.address.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_rating_string(self, data):
        """Assert for #App:9876:tc.address.rating!String."""
        output_var = '#App:9876:tc.address.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.address.security_label!StringArray."""
        output_var = '#App:9876:tc.address.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_source_string(self, data):
        """Assert for #App:9876:tc.address.source!String."""
        output_var = '#App:9876:tc.address.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_success_count_string(self, data):
        """Assert for #App:9876:tc.address.success.count!String."""
        output_var = '#App:9876:tc.address.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_tag_stringarray(self, data):
        """Assert for #App:9876:tc.address.tag!StringArray."""
        output_var = '#App:9876:tc.address.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_tcentity(self, data):
        """Assert for #App:9876:tc.address!TCEntity."""
        output_var = '#App:9876:tc.address!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.address.threat_assess_score!String."""
        output_var = '#App:9876:tc.address.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_address_web_link_string(self, data):
        """Assert for #App:9876:tc.address.web_link!String."""
        output_var = '#App:9876:tc.address.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_action_string(self, data):
        """Assert for #App:9876:tc.addresses.action!String."""
        output_var = '#App:9876:tc.addresses.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.active_locked!StringArray."""
        output_var = '#App:9876:tc.addresses.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_active_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.active!StringArray."""
        output_var = '#App:9876:tc.addresses.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.confidence!StringArray."""
        output_var = '#App:9876:tc.addresses.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_count_string(self, data):
        """Assert for #App:9876:tc.addresses.count!String."""
        output_var = '#App:9876:tc.addresses.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.date_added!StringArray."""
        output_var = '#App:9876:tc.addresses.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_fail_count_string(self, data):
        """Assert for #App:9876:tc.addresses.fail.count!String."""
        output_var = '#App:9876:tc.addresses.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.addresses.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.addresses.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_id_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.id!StringArray."""
        output_var = '#App:9876:tc.addresses.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_ip_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.ip!StringArray."""
        output_var = '#App:9876:tc.addresses.ip!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_json_raw_string(self, data):
        """Assert for #App:9876:tc.addresses.json.raw!String."""
        output_var = '#App:9876:tc.addresses.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.last_modified!StringArray."""
        output_var = '#App:9876:tc.addresses.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.last_observed!StringArray."""
        output_var = '#App:9876:tc.addresses.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.observation_count!StringArray."""
        output_var = '#App:9876:tc.addresses.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.owner_name!StringArray."""
        output_var = '#App:9876:tc.addresses.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.private_flag!StringArray."""
        output_var = '#App:9876:tc.addresses.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_rating_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.rating!StringArray."""
        output_var = '#App:9876:tc.addresses.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_source_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.source!StringArray."""
        output_var = '#App:9876:tc.addresses.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_success_count_string(self, data):
        """Assert for #App:9876:tc.addresses.success.count!String."""
        output_var = '#App:9876:tc.addresses.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_tcentityarray(self, data):
        """Assert for #App:9876:tc.addresses!TCEntityArray."""
        output_var = '#App:9876:tc.addresses!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.addresses.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_addresses_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.addresses.web_link!StringArray."""
        output_var = '#App:9876:tc.addresses.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_action_string(self, data):
        """Assert for #App:9876:tc.asn.action!String."""
        output_var = '#App:9876:tc.asn.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_active_locked_string(self, data):
        """Assert for #App:9876:tc.asn.active_locked!String."""
        output_var = '#App:9876:tc.asn.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_active_string(self, data):
        """Assert for #App:9876:tc.asn.active!String."""
        output_var = '#App:9876:tc.asn.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_as_number_string(self, data):
        """Assert for #App:9876:tc.asn.as_number!String."""
        output_var = '#App:9876:tc.asn.as_number!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.asn.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.asn.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_confidence_string(self, data):
        """Assert for #App:9876:tc.asn.confidence!String."""
        output_var = '#App:9876:tc.asn.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_count_string(self, data):
        """Assert for #App:9876:tc.asn.count!String."""
        output_var = '#App:9876:tc.asn.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_date_added_string(self, data):
        """Assert for #App:9876:tc.asn.date_added!String."""
        output_var = '#App:9876:tc.asn.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_fail_count_string(self, data):
        """Assert for #App:9876:tc.asn.fail.count!String."""
        output_var = '#App:9876:tc.asn.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.asn.false_positive_count!String."""
        output_var = '#App:9876:tc.asn.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.asn.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.asn.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_id_string(self, data):
        """Assert for #App:9876:tc.asn.id!String."""
        output_var = '#App:9876:tc.asn.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_json_raw_string(self, data):
        """Assert for #App:9876:tc.asn.json.raw!String."""
        output_var = '#App:9876:tc.asn.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_last_modified_string(self, data):
        """Assert for #App:9876:tc.asn.last_modified!String."""
        output_var = '#App:9876:tc.asn.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_last_observed_string(self, data):
        """Assert for #App:9876:tc.asn.last_observed!String."""
        output_var = '#App:9876:tc.asn.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_observation_count_string(self, data):
        """Assert for #App:9876:tc.asn.observation_count!String."""
        output_var = '#App:9876:tc.asn.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_owner_name_string(self, data):
        """Assert for #App:9876:tc.asn.owner_name!String."""
        output_var = '#App:9876:tc.asn.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_private_flag_string(self, data):
        """Assert for #App:9876:tc.asn.private_flag!String."""
        output_var = '#App:9876:tc.asn.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_rating_string(self, data):
        """Assert for #App:9876:tc.asn.rating!String."""
        output_var = '#App:9876:tc.asn.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.asn.security_label!StringArray."""
        output_var = '#App:9876:tc.asn.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_source_string(self, data):
        """Assert for #App:9876:tc.asn.source!String."""
        output_var = '#App:9876:tc.asn.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_success_count_string(self, data):
        """Assert for #App:9876:tc.asn.success.count!String."""
        output_var = '#App:9876:tc.asn.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_tag_stringarray(self, data):
        """Assert for #App:9876:tc.asn.tag!StringArray."""
        output_var = '#App:9876:tc.asn.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_tcentity(self, data):
        """Assert for #App:9876:tc.asn!TCEntity."""
        output_var = '#App:9876:tc.asn!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.asn.threat_assess_score!String."""
        output_var = '#App:9876:tc.asn.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asn_web_link_string(self, data):
        """Assert for #App:9876:tc.asn.web_link!String."""
        output_var = '#App:9876:tc.asn.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_action_string(self, data):
        """Assert for #App:9876:tc.asns.action!String."""
        output_var = '#App:9876:tc.asns.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.asns.active_locked!StringArray."""
        output_var = '#App:9876:tc.asns.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_active_stringarray(self, data):
        """Assert for #App:9876:tc.asns.active!StringArray."""
        output_var = '#App:9876:tc.asns.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_as_number_stringarray(self, data):
        """Assert for #App:9876:tc.asns.as_number!StringArray."""
        output_var = '#App:9876:tc.asns.as_number!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.asns.confidence!StringArray."""
        output_var = '#App:9876:tc.asns.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_count_string(self, data):
        """Assert for #App:9876:tc.asns.count!String."""
        output_var = '#App:9876:tc.asns.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.asns.date_added!StringArray."""
        output_var = '#App:9876:tc.asns.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_fail_count_string(self, data):
        """Assert for #App:9876:tc.asns.fail.count!String."""
        output_var = '#App:9876:tc.asns.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.asns.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.asns.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.asns.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.asns.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_id_stringarray(self, data):
        """Assert for #App:9876:tc.asns.id!StringArray."""
        output_var = '#App:9876:tc.asns.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_json_raw_string(self, data):
        """Assert for #App:9876:tc.asns.json.raw!String."""
        output_var = '#App:9876:tc.asns.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.asns.last_modified!StringArray."""
        output_var = '#App:9876:tc.asns.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.asns.last_observed!StringArray."""
        output_var = '#App:9876:tc.asns.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.asns.observation_count!StringArray."""
        output_var = '#App:9876:tc.asns.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.asns.owner_name!StringArray."""
        output_var = '#App:9876:tc.asns.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.asns.private_flag!StringArray."""
        output_var = '#App:9876:tc.asns.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_rating_stringarray(self, data):
        """Assert for #App:9876:tc.asns.rating!StringArray."""
        output_var = '#App:9876:tc.asns.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_source_stringarray(self, data):
        """Assert for #App:9876:tc.asns.source!StringArray."""
        output_var = '#App:9876:tc.asns.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_success_count_string(self, data):
        """Assert for #App:9876:tc.asns.success.count!String."""
        output_var = '#App:9876:tc.asns.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_tcentityarray(self, data):
        """Assert for #App:9876:tc.asns!TCEntityArray."""
        output_var = '#App:9876:tc.asns!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.asns.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.asns.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_asns_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.asns.web_link!StringArray."""
        output_var = '#App:9876:tc.asns.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_action_string(self, data):
        """Assert for #App:9876:tc.cidr_block.action!String."""
        output_var = '#App:9876:tc.cidr_block.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_active_locked_string(self, data):
        """Assert for #App:9876:tc.cidr_block.active_locked!String."""
        output_var = '#App:9876:tc.cidr_block.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_active_string(self, data):
        """Assert for #App:9876:tc.cidr_block.active!String."""
        output_var = '#App:9876:tc.cidr_block.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.cidr_block.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.cidr_block.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_block_string(self, data):
        """Assert for #App:9876:tc.cidr_block.block!String."""
        output_var = '#App:9876:tc.cidr_block.block!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_confidence_string(self, data):
        """Assert for #App:9876:tc.cidr_block.confidence!String."""
        output_var = '#App:9876:tc.cidr_block.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_count_string(self, data):
        """Assert for #App:9876:tc.cidr_block.count!String."""
        output_var = '#App:9876:tc.cidr_block.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_date_added_string(self, data):
        """Assert for #App:9876:tc.cidr_block.date_added!String."""
        output_var = '#App:9876:tc.cidr_block.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_fail_count_string(self, data):
        """Assert for #App:9876:tc.cidr_block.fail.count!String."""
        output_var = '#App:9876:tc.cidr_block.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.cidr_block.false_positive_count!String."""
        output_var = '#App:9876:tc.cidr_block.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.cidr_block.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.cidr_block.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_id_string(self, data):
        """Assert for #App:9876:tc.cidr_block.id!String."""
        output_var = '#App:9876:tc.cidr_block.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_json_raw_string(self, data):
        """Assert for #App:9876:tc.cidr_block.json.raw!String."""
        output_var = '#App:9876:tc.cidr_block.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_last_modified_string(self, data):
        """Assert for #App:9876:tc.cidr_block.last_modified!String."""
        output_var = '#App:9876:tc.cidr_block.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_last_observed_string(self, data):
        """Assert for #App:9876:tc.cidr_block.last_observed!String."""
        output_var = '#App:9876:tc.cidr_block.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_observation_count_string(self, data):
        """Assert for #App:9876:tc.cidr_block.observation_count!String."""
        output_var = '#App:9876:tc.cidr_block.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_owner_name_string(self, data):
        """Assert for #App:9876:tc.cidr_block.owner_name!String."""
        output_var = '#App:9876:tc.cidr_block.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_private_flag_string(self, data):
        """Assert for #App:9876:tc.cidr_block.private_flag!String."""
        output_var = '#App:9876:tc.cidr_block.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_rating_string(self, data):
        """Assert for #App:9876:tc.cidr_block.rating!String."""
        output_var = '#App:9876:tc.cidr_block.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_block.security_label!StringArray."""
        output_var = '#App:9876:tc.cidr_block.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_source_string(self, data):
        """Assert for #App:9876:tc.cidr_block.source!String."""
        output_var = '#App:9876:tc.cidr_block.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_success_count_string(self, data):
        """Assert for #App:9876:tc.cidr_block.success.count!String."""
        output_var = '#App:9876:tc.cidr_block.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_tag_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_block.tag!StringArray."""
        output_var = '#App:9876:tc.cidr_block.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_tcentity(self, data):
        """Assert for #App:9876:tc.cidr_block!TCEntity."""
        output_var = '#App:9876:tc.cidr_block!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.cidr_block.threat_assess_score!String."""
        output_var = '#App:9876:tc.cidr_block.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_block_web_link_string(self, data):
        """Assert for #App:9876:tc.cidr_block.web_link!String."""
        output_var = '#App:9876:tc.cidr_block.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_action_string(self, data):
        """Assert for #App:9876:tc.cidr_blocks.action!String."""
        output_var = '#App:9876:tc.cidr_blocks.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.active_locked!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_active_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.active!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_block_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.block!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.block!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.confidence!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_count_string(self, data):
        """Assert for #App:9876:tc.cidr_blocks.count!String."""
        output_var = '#App:9876:tc.cidr_blocks.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.date_added!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_fail_count_string(self, data):
        """Assert for #App:9876:tc.cidr_blocks.fail.count!String."""
        output_var = '#App:9876:tc.cidr_blocks.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_id_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.id!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_json_raw_string(self, data):
        """Assert for #App:9876:tc.cidr_blocks.json.raw!String."""
        output_var = '#App:9876:tc.cidr_blocks.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.last_modified!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.last_observed!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.observation_count!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.owner_name!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.private_flag!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_rating_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.rating!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_source_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.source!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_success_count_string(self, data):
        """Assert for #App:9876:tc.cidr_blocks.success.count!String."""
        output_var = '#App:9876:tc.cidr_blocks.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_tcentityarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks!TCEntityArray."""
        output_var = '#App:9876:tc.cidr_blocks!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_cidr_blocks_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.cidr_blocks.web_link!StringArray."""
        output_var = '#App:9876:tc.cidr_blocks.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_action_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.action!String."""
        output_var = '#App:9876:tc.custom_indicator.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_active_locked_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.active_locked!String."""
        output_var = '#App:9876:tc.custom_indicator.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_active_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.active!String."""
        output_var = '#App:9876:tc.custom_indicator.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.custom_indicator.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.custom_indicator.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_confidence_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.confidence!String."""
        output_var = '#App:9876:tc.custom_indicator.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.count!String."""
        output_var = '#App:9876:tc.custom_indicator.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_date_added_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.date_added!String."""
        output_var = '#App:9876:tc.custom_indicator.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_fail_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.fail.count!String."""
        output_var = '#App:9876:tc.custom_indicator.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.false_positive_count!String."""
        output_var = '#App:9876:tc.custom_indicator.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.custom_indicator.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_id_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.id!String."""
        output_var = '#App:9876:tc.custom_indicator.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_json_raw_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.json.raw!String."""
        output_var = '#App:9876:tc.custom_indicator.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_last_modified_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.last_modified!String."""
        output_var = '#App:9876:tc.custom_indicator.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_last_observed_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.last_observed!String."""
        output_var = '#App:9876:tc.custom_indicator.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_observation_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.observation_count!String."""
        output_var = '#App:9876:tc.custom_indicator.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_owner_name_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.owner_name!String."""
        output_var = '#App:9876:tc.custom_indicator.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_private_flag_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.private_flag!String."""
        output_var = '#App:9876:tc.custom_indicator.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_rating_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.rating!String."""
        output_var = '#App:9876:tc.custom_indicator.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicator.security_label!StringArray."""
        output_var = '#App:9876:tc.custom_indicator.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_source_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.source!String."""
        output_var = '#App:9876:tc.custom_indicator.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_success_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.success.count!String."""
        output_var = '#App:9876:tc.custom_indicator.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_tag_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicator.tag!StringArray."""
        output_var = '#App:9876:tc.custom_indicator.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_tcentity(self, data):
        """Assert for #App:9876:tc.custom_indicator!TCEntity."""
        output_var = '#App:9876:tc.custom_indicator!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.threat_assess_score!String."""
        output_var = '#App:9876:tc.custom_indicator.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_value_1_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.value_1!String."""
        output_var = '#App:9876:tc.custom_indicator.value_1!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_value_2_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.value_2!String."""
        output_var = '#App:9876:tc.custom_indicator.value_2!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_value_3_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.value_3!String."""
        output_var = '#App:9876:tc.custom_indicator.value_3!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicator_web_link_string(self, data):
        """Assert for #App:9876:tc.custom_indicator.web_link!String."""
        output_var = '#App:9876:tc.custom_indicator.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_action_string(self, data):
        """Assert for #App:9876:tc.custom_indicators.action!String."""
        output_var = '#App:9876:tc.custom_indicators.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.active_locked!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_active_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.active!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.confidence!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicators.count!String."""
        output_var = '#App:9876:tc.custom_indicators.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.date_added!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_fail_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicators.fail.count!String."""
        output_var = '#App:9876:tc.custom_indicators.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_id_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.id!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_json_raw_string(self, data):
        """Assert for #App:9876:tc.custom_indicators.json.raw!String."""
        output_var = '#App:9876:tc.custom_indicators.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.last_modified!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.last_observed!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.observation_count!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.owner_name!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.private_flag!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_rating_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.rating!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_source_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.source!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_success_count_string(self, data):
        """Assert for #App:9876:tc.custom_indicators.success.count!String."""
        output_var = '#App:9876:tc.custom_indicators.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_tcentityarray(self, data):
        """Assert for #App:9876:tc.custom_indicators!TCEntityArray."""
        output_var = '#App:9876:tc.custom_indicators!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_value_1_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.value_1!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.value_1!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_value_2_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.value_2!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.value_2!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_value_3_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.value_3!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.value_3!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_custom_indicators_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.custom_indicators.web_link!StringArray."""
        output_var = '#App:9876:tc.custom_indicators.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_action_string(self, data):
        """Assert for #App:9876:tc.email_address.action!String."""
        output_var = '#App:9876:tc.email_address.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_active_locked_string(self, data):
        """Assert for #App:9876:tc.email_address.active_locked!String."""
        output_var = '#App:9876:tc.email_address.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_active_string(self, data):
        """Assert for #App:9876:tc.email_address.active!String."""
        output_var = '#App:9876:tc.email_address.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_address_string(self, data):
        """Assert for #App:9876:tc.email_address.address!String."""
        output_var = '#App:9876:tc.email_address.address!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.email_address.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.email_address.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_confidence_string(self, data):
        """Assert for #App:9876:tc.email_address.confidence!String."""
        output_var = '#App:9876:tc.email_address.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_count_string(self, data):
        """Assert for #App:9876:tc.email_address.count!String."""
        output_var = '#App:9876:tc.email_address.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_date_added_string(self, data):
        """Assert for #App:9876:tc.email_address.date_added!String."""
        output_var = '#App:9876:tc.email_address.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_fail_count_string(self, data):
        """Assert for #App:9876:tc.email_address.fail.count!String."""
        output_var = '#App:9876:tc.email_address.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.email_address.false_positive_count!String."""
        output_var = '#App:9876:tc.email_address.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.email_address.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.email_address.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_id_string(self, data):
        """Assert for #App:9876:tc.email_address.id!String."""
        output_var = '#App:9876:tc.email_address.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_json_raw_string(self, data):
        """Assert for #App:9876:tc.email_address.json.raw!String."""
        output_var = '#App:9876:tc.email_address.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_last_modified_string(self, data):
        """Assert for #App:9876:tc.email_address.last_modified!String."""
        output_var = '#App:9876:tc.email_address.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_last_observed_string(self, data):
        """Assert for #App:9876:tc.email_address.last_observed!String."""
        output_var = '#App:9876:tc.email_address.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_observation_count_string(self, data):
        """Assert for #App:9876:tc.email_address.observation_count!String."""
        output_var = '#App:9876:tc.email_address.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_owner_name_string(self, data):
        """Assert for #App:9876:tc.email_address.owner_name!String."""
        output_var = '#App:9876:tc.email_address.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_private_flag_string(self, data):
        """Assert for #App:9876:tc.email_address.private_flag!String."""
        output_var = '#App:9876:tc.email_address.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_rating_string(self, data):
        """Assert for #App:9876:tc.email_address.rating!String."""
        output_var = '#App:9876:tc.email_address.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.email_address.security_label!StringArray."""
        output_var = '#App:9876:tc.email_address.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_source_string(self, data):
        """Assert for #App:9876:tc.email_address.source!String."""
        output_var = '#App:9876:tc.email_address.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_success_count_string(self, data):
        """Assert for #App:9876:tc.email_address.success.count!String."""
        output_var = '#App:9876:tc.email_address.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_tag_stringarray(self, data):
        """Assert for #App:9876:tc.email_address.tag!StringArray."""
        output_var = '#App:9876:tc.email_address.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_tcentity(self, data):
        """Assert for #App:9876:tc.email_address!TCEntity."""
        output_var = '#App:9876:tc.email_address!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.email_address.threat_assess_score!String."""
        output_var = '#App:9876:tc.email_address.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_address_web_link_string(self, data):
        """Assert for #App:9876:tc.email_address.web_link!String."""
        output_var = '#App:9876:tc.email_address.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_action_string(self, data):
        """Assert for #App:9876:tc.email_addresses.action!String."""
        output_var = '#App:9876:tc.email_addresses.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.active_locked!StringArray."""
        output_var = '#App:9876:tc.email_addresses.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_active_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.active!StringArray."""
        output_var = '#App:9876:tc.email_addresses.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_address_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.address!StringArray."""
        output_var = '#App:9876:tc.email_addresses.address!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.confidence!StringArray."""
        output_var = '#App:9876:tc.email_addresses.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_count_string(self, data):
        """Assert for #App:9876:tc.email_addresses.count!String."""
        output_var = '#App:9876:tc.email_addresses.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.date_added!StringArray."""
        output_var = '#App:9876:tc.email_addresses.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_fail_count_string(self, data):
        """Assert for #App:9876:tc.email_addresses.fail.count!String."""
        output_var = '#App:9876:tc.email_addresses.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.email_addresses.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.email_addresses.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_id_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.id!StringArray."""
        output_var = '#App:9876:tc.email_addresses.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_json_raw_string(self, data):
        """Assert for #App:9876:tc.email_addresses.json.raw!String."""
        output_var = '#App:9876:tc.email_addresses.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.last_modified!StringArray."""
        output_var = '#App:9876:tc.email_addresses.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.last_observed!StringArray."""
        output_var = '#App:9876:tc.email_addresses.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.observation_count!StringArray."""
        output_var = '#App:9876:tc.email_addresses.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.owner_name!StringArray."""
        output_var = '#App:9876:tc.email_addresses.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.private_flag!StringArray."""
        output_var = '#App:9876:tc.email_addresses.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_rating_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.rating!StringArray."""
        output_var = '#App:9876:tc.email_addresses.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_source_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.source!StringArray."""
        output_var = '#App:9876:tc.email_addresses.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_success_count_string(self, data):
        """Assert for #App:9876:tc.email_addresses.success.count!String."""
        output_var = '#App:9876:tc.email_addresses.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_tcentityarray(self, data):
        """Assert for #App:9876:tc.email_addresses!TCEntityArray."""
        output_var = '#App:9876:tc.email_addresses!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.email_addresses.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_addresses_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.email_addresses.web_link!StringArray."""
        output_var = '#App:9876:tc.email_addresses.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_action_string(self, data):
        """Assert for #App:9876:tc.email_subject.action!String."""
        output_var = '#App:9876:tc.email_subject.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_active_locked_string(self, data):
        """Assert for #App:9876:tc.email_subject.active_locked!String."""
        output_var = '#App:9876:tc.email_subject.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_active_string(self, data):
        """Assert for #App:9876:tc.email_subject.active!String."""
        output_var = '#App:9876:tc.email_subject.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.email_subject.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.email_subject.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_confidence_string(self, data):
        """Assert for #App:9876:tc.email_subject.confidence!String."""
        output_var = '#App:9876:tc.email_subject.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_count_string(self, data):
        """Assert for #App:9876:tc.email_subject.count!String."""
        output_var = '#App:9876:tc.email_subject.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_date_added_string(self, data):
        """Assert for #App:9876:tc.email_subject.date_added!String."""
        output_var = '#App:9876:tc.email_subject.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_fail_count_string(self, data):
        """Assert for #App:9876:tc.email_subject.fail.count!String."""
        output_var = '#App:9876:tc.email_subject.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.email_subject.false_positive_count!String."""
        output_var = '#App:9876:tc.email_subject.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.email_subject.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.email_subject.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_id_string(self, data):
        """Assert for #App:9876:tc.email_subject.id!String."""
        output_var = '#App:9876:tc.email_subject.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_json_raw_string(self, data):
        """Assert for #App:9876:tc.email_subject.json.raw!String."""
        output_var = '#App:9876:tc.email_subject.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_last_modified_string(self, data):
        """Assert for #App:9876:tc.email_subject.last_modified!String."""
        output_var = '#App:9876:tc.email_subject.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_last_observed_string(self, data):
        """Assert for #App:9876:tc.email_subject.last_observed!String."""
        output_var = '#App:9876:tc.email_subject.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_observation_count_string(self, data):
        """Assert for #App:9876:tc.email_subject.observation_count!String."""
        output_var = '#App:9876:tc.email_subject.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_owner_name_string(self, data):
        """Assert for #App:9876:tc.email_subject.owner_name!String."""
        output_var = '#App:9876:tc.email_subject.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_private_flag_string(self, data):
        """Assert for #App:9876:tc.email_subject.private_flag!String."""
        output_var = '#App:9876:tc.email_subject.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_rating_string(self, data):
        """Assert for #App:9876:tc.email_subject.rating!String."""
        output_var = '#App:9876:tc.email_subject.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.email_subject.security_label!StringArray."""
        output_var = '#App:9876:tc.email_subject.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_source_string(self, data):
        """Assert for #App:9876:tc.email_subject.source!String."""
        output_var = '#App:9876:tc.email_subject.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_subject_string(self, data):
        """Assert for #App:9876:tc.email_subject.subject!String."""
        output_var = '#App:9876:tc.email_subject.subject!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_success_count_string(self, data):
        """Assert for #App:9876:tc.email_subject.success.count!String."""
        output_var = '#App:9876:tc.email_subject.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_tag_stringarray(self, data):
        """Assert for #App:9876:tc.email_subject.tag!StringArray."""
        output_var = '#App:9876:tc.email_subject.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_tcentity(self, data):
        """Assert for #App:9876:tc.email_subject!TCEntity."""
        output_var = '#App:9876:tc.email_subject!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.email_subject.threat_assess_score!String."""
        output_var = '#App:9876:tc.email_subject.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subject_web_link_string(self, data):
        """Assert for #App:9876:tc.email_subject.web_link!String."""
        output_var = '#App:9876:tc.email_subject.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_action_string(self, data):
        """Assert for #App:9876:tc.email_subjects.action!String."""
        output_var = '#App:9876:tc.email_subjects.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.active_locked!StringArray."""
        output_var = '#App:9876:tc.email_subjects.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_active_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.active!StringArray."""
        output_var = '#App:9876:tc.email_subjects.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.confidence!StringArray."""
        output_var = '#App:9876:tc.email_subjects.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_count_string(self, data):
        """Assert for #App:9876:tc.email_subjects.count!String."""
        output_var = '#App:9876:tc.email_subjects.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.date_added!StringArray."""
        output_var = '#App:9876:tc.email_subjects.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_fail_count_string(self, data):
        """Assert for #App:9876:tc.email_subjects.fail.count!String."""
        output_var = '#App:9876:tc.email_subjects.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.email_subjects.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.email_subjects.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_id_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.id!StringArray."""
        output_var = '#App:9876:tc.email_subjects.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_json_raw_string(self, data):
        """Assert for #App:9876:tc.email_subjects.json.raw!String."""
        output_var = '#App:9876:tc.email_subjects.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.last_modified!StringArray."""
        output_var = '#App:9876:tc.email_subjects.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.last_observed!StringArray."""
        output_var = '#App:9876:tc.email_subjects.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.observation_count!StringArray."""
        output_var = '#App:9876:tc.email_subjects.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.owner_name!StringArray."""
        output_var = '#App:9876:tc.email_subjects.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.private_flag!StringArray."""
        output_var = '#App:9876:tc.email_subjects.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_rating_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.rating!StringArray."""
        output_var = '#App:9876:tc.email_subjects.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_source_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.source!StringArray."""
        output_var = '#App:9876:tc.email_subjects.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_subject_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.subject!StringArray."""
        output_var = '#App:9876:tc.email_subjects.subject!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_success_count_string(self, data):
        """Assert for #App:9876:tc.email_subjects.success.count!String."""
        output_var = '#App:9876:tc.email_subjects.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_tcentityarray(self, data):
        """Assert for #App:9876:tc.email_subjects!TCEntityArray."""
        output_var = '#App:9876:tc.email_subjects!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.email_subjects.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_email_subjects_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.email_subjects.web_link!StringArray."""
        output_var = '#App:9876:tc.email_subjects.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_action_string(self, data):
        """Assert for #App:9876:tc.file.action!String."""
        output_var = '#App:9876:tc.file.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_active_locked_string(self, data):
        """Assert for #App:9876:tc.file.active_locked!String."""
        output_var = '#App:9876:tc.file.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_active_string(self, data):
        """Assert for #App:9876:tc.file.active!String."""
        output_var = '#App:9876:tc.file.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.file.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.file.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_confidence_string(self, data):
        """Assert for #App:9876:tc.file.confidence!String."""
        output_var = '#App:9876:tc.file.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_count_string(self, data):
        """Assert for #App:9876:tc.file.count!String."""
        output_var = '#App:9876:tc.file.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_date_added_string(self, data):
        """Assert for #App:9876:tc.file.date_added!String."""
        output_var = '#App:9876:tc.file.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_fail_count_string(self, data):
        """Assert for #App:9876:tc.file.fail.count!String."""
        output_var = '#App:9876:tc.file.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.file.false_positive_count!String."""
        output_var = '#App:9876:tc.file.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.file.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.file.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_id_string(self, data):
        """Assert for #App:9876:tc.file.id!String."""
        output_var = '#App:9876:tc.file.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_json_raw_string(self, data):
        """Assert for #App:9876:tc.file.json.raw!String."""
        output_var = '#App:9876:tc.file.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_last_modified_string(self, data):
        """Assert for #App:9876:tc.file.last_modified!String."""
        output_var = '#App:9876:tc.file.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_last_observed_string(self, data):
        """Assert for #App:9876:tc.file.last_observed!String."""
        output_var = '#App:9876:tc.file.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_md5_string(self, data):
        """Assert for #App:9876:tc.file.md5!String."""
        output_var = '#App:9876:tc.file.md5!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_observation_count_string(self, data):
        """Assert for #App:9876:tc.file.observation_count!String."""
        output_var = '#App:9876:tc.file.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_owner_name_string(self, data):
        """Assert for #App:9876:tc.file.owner_name!String."""
        output_var = '#App:9876:tc.file.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_private_flag_string(self, data):
        """Assert for #App:9876:tc.file.private_flag!String."""
        output_var = '#App:9876:tc.file.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_rating_string(self, data):
        """Assert for #App:9876:tc.file.rating!String."""
        output_var = '#App:9876:tc.file.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.file.security_label!StringArray."""
        output_var = '#App:9876:tc.file.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_sha1_string(self, data):
        """Assert for #App:9876:tc.file.sha1!String."""
        output_var = '#App:9876:tc.file.sha1!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_sha256_string(self, data):
        """Assert for #App:9876:tc.file.sha256!String."""
        output_var = '#App:9876:tc.file.sha256!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_size_string(self, data):
        """Assert for #App:9876:tc.file.size!String."""
        output_var = '#App:9876:tc.file.size!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_source_string(self, data):
        """Assert for #App:9876:tc.file.source!String."""
        output_var = '#App:9876:tc.file.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_success_count_string(self, data):
        """Assert for #App:9876:tc.file.success.count!String."""
        output_var = '#App:9876:tc.file.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_tag_stringarray(self, data):
        """Assert for #App:9876:tc.file.tag!StringArray."""
        output_var = '#App:9876:tc.file.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_tcentity(self, data):
        """Assert for #App:9876:tc.file!TCEntity."""
        output_var = '#App:9876:tc.file!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.file.threat_assess_score!String."""
        output_var = '#App:9876:tc.file.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_file_web_link_string(self, data):
        """Assert for #App:9876:tc.file.web_link!String."""
        output_var = '#App:9876:tc.file.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_action_string(self, data):
        """Assert for #App:9876:tc.files.action!String."""
        output_var = '#App:9876:tc.files.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.files.active_locked!StringArray."""
        output_var = '#App:9876:tc.files.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_active_stringarray(self, data):
        """Assert for #App:9876:tc.files.active!StringArray."""
        output_var = '#App:9876:tc.files.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.files.confidence!StringArray."""
        output_var = '#App:9876:tc.files.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_count_string(self, data):
        """Assert for #App:9876:tc.files.count!String."""
        output_var = '#App:9876:tc.files.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.files.date_added!StringArray."""
        output_var = '#App:9876:tc.files.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_fail_count_string(self, data):
        """Assert for #App:9876:tc.files.fail.count!String."""
        output_var = '#App:9876:tc.files.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.files.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.files.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.files.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.files.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_id_stringarray(self, data):
        """Assert for #App:9876:tc.files.id!StringArray."""
        output_var = '#App:9876:tc.files.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_json_raw_string(self, data):
        """Assert for #App:9876:tc.files.json.raw!String."""
        output_var = '#App:9876:tc.files.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.files.last_modified!StringArray."""
        output_var = '#App:9876:tc.files.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.files.last_observed!StringArray."""
        output_var = '#App:9876:tc.files.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_md5_stringarray(self, data):
        """Assert for #App:9876:tc.files.md5!StringArray."""
        output_var = '#App:9876:tc.files.md5!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.files.observation_count!StringArray."""
        output_var = '#App:9876:tc.files.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.files.owner_name!StringArray."""
        output_var = '#App:9876:tc.files.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.files.private_flag!StringArray."""
        output_var = '#App:9876:tc.files.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_rating_stringarray(self, data):
        """Assert for #App:9876:tc.files.rating!StringArray."""
        output_var = '#App:9876:tc.files.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_sha1_stringarray(self, data):
        """Assert for #App:9876:tc.files.sha1!StringArray."""
        output_var = '#App:9876:tc.files.sha1!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_sha256_stringarray(self, data):
        """Assert for #App:9876:tc.files.sha256!StringArray."""
        output_var = '#App:9876:tc.files.sha256!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_size_stringarray(self, data):
        """Assert for #App:9876:tc.files.size!StringArray."""
        output_var = '#App:9876:tc.files.size!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_source_stringarray(self, data):
        """Assert for #App:9876:tc.files.source!StringArray."""
        output_var = '#App:9876:tc.files.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_success_count_string(self, data):
        """Assert for #App:9876:tc.files.success.count!String."""
        output_var = '#App:9876:tc.files.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_tcentityarray(self, data):
        """Assert for #App:9876:tc.files!TCEntityArray."""
        output_var = '#App:9876:tc.files!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.files.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.files.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_files_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.files.web_link!StringArray."""
        output_var = '#App:9876:tc.files.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_action_string(self, data):
        """Assert for #App:9876:tc.hashtag.action!String."""
        output_var = '#App:9876:tc.hashtag.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_active_locked_string(self, data):
        """Assert for #App:9876:tc.hashtag.active_locked!String."""
        output_var = '#App:9876:tc.hashtag.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_active_string(self, data):
        """Assert for #App:9876:tc.hashtag.active!String."""
        output_var = '#App:9876:tc.hashtag.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.hashtag.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.hashtag.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_confidence_string(self, data):
        """Assert for #App:9876:tc.hashtag.confidence!String."""
        output_var = '#App:9876:tc.hashtag.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_count_string(self, data):
        """Assert for #App:9876:tc.hashtag.count!String."""
        output_var = '#App:9876:tc.hashtag.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_date_added_string(self, data):
        """Assert for #App:9876:tc.hashtag.date_added!String."""
        output_var = '#App:9876:tc.hashtag.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_fail_count_string(self, data):
        """Assert for #App:9876:tc.hashtag.fail.count!String."""
        output_var = '#App:9876:tc.hashtag.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.hashtag.false_positive_count!String."""
        output_var = '#App:9876:tc.hashtag.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.hashtag.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.hashtag.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_hashtag_string(self, data):
        """Assert for #App:9876:tc.hashtag.hashtag!String."""
        output_var = '#App:9876:tc.hashtag.hashtag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_id_string(self, data):
        """Assert for #App:9876:tc.hashtag.id!String."""
        output_var = '#App:9876:tc.hashtag.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_json_raw_string(self, data):
        """Assert for #App:9876:tc.hashtag.json.raw!String."""
        output_var = '#App:9876:tc.hashtag.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_last_modified_string(self, data):
        """Assert for #App:9876:tc.hashtag.last_modified!String."""
        output_var = '#App:9876:tc.hashtag.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_last_observed_string(self, data):
        """Assert for #App:9876:tc.hashtag.last_observed!String."""
        output_var = '#App:9876:tc.hashtag.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_observation_count_string(self, data):
        """Assert for #App:9876:tc.hashtag.observation_count!String."""
        output_var = '#App:9876:tc.hashtag.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_owner_name_string(self, data):
        """Assert for #App:9876:tc.hashtag.owner_name!String."""
        output_var = '#App:9876:tc.hashtag.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_private_flag_string(self, data):
        """Assert for #App:9876:tc.hashtag.private_flag!String."""
        output_var = '#App:9876:tc.hashtag.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_rating_string(self, data):
        """Assert for #App:9876:tc.hashtag.rating!String."""
        output_var = '#App:9876:tc.hashtag.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.hashtag.security_label!StringArray."""
        output_var = '#App:9876:tc.hashtag.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_source_string(self, data):
        """Assert for #App:9876:tc.hashtag.source!String."""
        output_var = '#App:9876:tc.hashtag.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_success_count_string(self, data):
        """Assert for #App:9876:tc.hashtag.success.count!String."""
        output_var = '#App:9876:tc.hashtag.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_tag_stringarray(self, data):
        """Assert for #App:9876:tc.hashtag.tag!StringArray."""
        output_var = '#App:9876:tc.hashtag.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_tcentity(self, data):
        """Assert for #App:9876:tc.hashtag!TCEntity."""
        output_var = '#App:9876:tc.hashtag!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.hashtag.threat_assess_score!String."""
        output_var = '#App:9876:tc.hashtag.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtag_web_link_string(self, data):
        """Assert for #App:9876:tc.hashtag.web_link!String."""
        output_var = '#App:9876:tc.hashtag.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_action_string(self, data):
        """Assert for #App:9876:tc.hashtags.action!String."""
        output_var = '#App:9876:tc.hashtags.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.active_locked!StringArray."""
        output_var = '#App:9876:tc.hashtags.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_active_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.active!StringArray."""
        output_var = '#App:9876:tc.hashtags.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.confidence!StringArray."""
        output_var = '#App:9876:tc.hashtags.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_count_string(self, data):
        """Assert for #App:9876:tc.hashtags.count!String."""
        output_var = '#App:9876:tc.hashtags.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.date_added!StringArray."""
        output_var = '#App:9876:tc.hashtags.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_fail_count_string(self, data):
        """Assert for #App:9876:tc.hashtags.fail.count!String."""
        output_var = '#App:9876:tc.hashtags.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.hashtags.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.hashtags.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_hashtag_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.hashtag!StringArray."""
        output_var = '#App:9876:tc.hashtags.hashtag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_id_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.id!StringArray."""
        output_var = '#App:9876:tc.hashtags.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_json_raw_string(self, data):
        """Assert for #App:9876:tc.hashtags.json.raw!String."""
        output_var = '#App:9876:tc.hashtags.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.last_modified!StringArray."""
        output_var = '#App:9876:tc.hashtags.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.last_observed!StringArray."""
        output_var = '#App:9876:tc.hashtags.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.observation_count!StringArray."""
        output_var = '#App:9876:tc.hashtags.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.owner_name!StringArray."""
        output_var = '#App:9876:tc.hashtags.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.private_flag!StringArray."""
        output_var = '#App:9876:tc.hashtags.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_rating_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.rating!StringArray."""
        output_var = '#App:9876:tc.hashtags.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_source_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.source!StringArray."""
        output_var = '#App:9876:tc.hashtags.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_success_count_string(self, data):
        """Assert for #App:9876:tc.hashtags.success.count!String."""
        output_var = '#App:9876:tc.hashtags.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_tcentityarray(self, data):
        """Assert for #App:9876:tc.hashtags!TCEntityArray."""
        output_var = '#App:9876:tc.hashtags!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.hashtags.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hashtags_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.hashtags.web_link!StringArray."""
        output_var = '#App:9876:tc.hashtags.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_action_string(self, data):
        """Assert for #App:9876:tc.host.action!String."""
        output_var = '#App:9876:tc.host.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_active_locked_string(self, data):
        """Assert for #App:9876:tc.host.active_locked!String."""
        output_var = '#App:9876:tc.host.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_active_string(self, data):
        """Assert for #App:9876:tc.host.active!String."""
        output_var = '#App:9876:tc.host.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.host.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.host.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_confidence_string(self, data):
        """Assert for #App:9876:tc.host.confidence!String."""
        output_var = '#App:9876:tc.host.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_count_string(self, data):
        """Assert for #App:9876:tc.host.count!String."""
        output_var = '#App:9876:tc.host.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_date_added_string(self, data):
        """Assert for #App:9876:tc.host.date_added!String."""
        output_var = '#App:9876:tc.host.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_dns_active_string(self, data):
        """Assert for #App:9876:tc.host.dns_active!String."""
        output_var = '#App:9876:tc.host.dns_active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_fail_count_string(self, data):
        """Assert for #App:9876:tc.host.fail.count!String."""
        output_var = '#App:9876:tc.host.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.host.false_positive_count!String."""
        output_var = '#App:9876:tc.host.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.host.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.host.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_host_name_string(self, data):
        """Assert for #App:9876:tc.host.host_name!String."""
        output_var = '#App:9876:tc.host.host_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_id_string(self, data):
        """Assert for #App:9876:tc.host.id!String."""
        output_var = '#App:9876:tc.host.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_json_raw_string(self, data):
        """Assert for #App:9876:tc.host.json.raw!String."""
        output_var = '#App:9876:tc.host.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_last_modified_string(self, data):
        """Assert for #App:9876:tc.host.last_modified!String."""
        output_var = '#App:9876:tc.host.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_last_observed_string(self, data):
        """Assert for #App:9876:tc.host.last_observed!String."""
        output_var = '#App:9876:tc.host.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_observation_count_string(self, data):
        """Assert for #App:9876:tc.host.observation_count!String."""
        output_var = '#App:9876:tc.host.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_owner_name_string(self, data):
        """Assert for #App:9876:tc.host.owner_name!String."""
        output_var = '#App:9876:tc.host.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_private_flag_string(self, data):
        """Assert for #App:9876:tc.host.private_flag!String."""
        output_var = '#App:9876:tc.host.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_rating_string(self, data):
        """Assert for #App:9876:tc.host.rating!String."""
        output_var = '#App:9876:tc.host.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.host.security_label!StringArray."""
        output_var = '#App:9876:tc.host.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_source_string(self, data):
        """Assert for #App:9876:tc.host.source!String."""
        output_var = '#App:9876:tc.host.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_success_count_string(self, data):
        """Assert for #App:9876:tc.host.success.count!String."""
        output_var = '#App:9876:tc.host.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_tag_stringarray(self, data):
        """Assert for #App:9876:tc.host.tag!StringArray."""
        output_var = '#App:9876:tc.host.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_tcentity(self, data):
        """Assert for #App:9876:tc.host!TCEntity."""
        output_var = '#App:9876:tc.host!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.host.threat_assess_score!String."""
        output_var = '#App:9876:tc.host.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_web_link_string(self, data):
        """Assert for #App:9876:tc.host.web_link!String."""
        output_var = '#App:9876:tc.host.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_host_whois_active_string(self, data):
        """Assert for #App:9876:tc.host.whois_active!String."""
        output_var = '#App:9876:tc.host.whois_active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_action_string(self, data):
        """Assert for #App:9876:tc.hosts.action!String."""
        output_var = '#App:9876:tc.hosts.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.active_locked!StringArray."""
        output_var = '#App:9876:tc.hosts.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_active_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.active!StringArray."""
        output_var = '#App:9876:tc.hosts.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.confidence!StringArray."""
        output_var = '#App:9876:tc.hosts.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_count_string(self, data):
        """Assert for #App:9876:tc.hosts.count!String."""
        output_var = '#App:9876:tc.hosts.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.date_added!StringArray."""
        output_var = '#App:9876:tc.hosts.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_dns_active_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.dns_active!StringArray."""
        output_var = '#App:9876:tc.hosts.dns_active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_fail_count_string(self, data):
        """Assert for #App:9876:tc.hosts.fail.count!String."""
        output_var = '#App:9876:tc.hosts.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.hosts.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.hosts.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_host_name_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.host_name!StringArray."""
        output_var = '#App:9876:tc.hosts.host_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_id_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.id!StringArray."""
        output_var = '#App:9876:tc.hosts.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_json_raw_string(self, data):
        """Assert for #App:9876:tc.hosts.json.raw!String."""
        output_var = '#App:9876:tc.hosts.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.last_modified!StringArray."""
        output_var = '#App:9876:tc.hosts.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.last_observed!StringArray."""
        output_var = '#App:9876:tc.hosts.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.observation_count!StringArray."""
        output_var = '#App:9876:tc.hosts.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.owner_name!StringArray."""
        output_var = '#App:9876:tc.hosts.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.private_flag!StringArray."""
        output_var = '#App:9876:tc.hosts.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_rating_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.rating!StringArray."""
        output_var = '#App:9876:tc.hosts.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_source_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.source!StringArray."""
        output_var = '#App:9876:tc.hosts.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_success_count_string(self, data):
        """Assert for #App:9876:tc.hosts.success.count!String."""
        output_var = '#App:9876:tc.hosts.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_tcentityarray(self, data):
        """Assert for #App:9876:tc.hosts!TCEntityArray."""
        output_var = '#App:9876:tc.hosts!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.hosts.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.web_link!StringArray."""
        output_var = '#App:9876:tc.hosts.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_hosts_whois_active_stringarray(self, data):
        """Assert for #App:9876:tc.hosts.whois_active!StringArray."""
        output_var = '#App:9876:tc.hosts.whois_active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_action_string(self, data):
        """Assert for #App:9876:tc.indicator.action!String."""
        output_var = '#App:9876:tc.indicator.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_active_locked_string(self, data):
        """Assert for #App:9876:tc.indicator.active_locked!String."""
        output_var = '#App:9876:tc.indicator.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_active_string(self, data):
        """Assert for #App:9876:tc.indicator.active!String."""
        output_var = '#App:9876:tc.indicator.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.indicator.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.indicator.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_confidence_string(self, data):
        """Assert for #App:9876:tc.indicator.confidence!String."""
        output_var = '#App:9876:tc.indicator.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_count_string(self, data):
        """Assert for #App:9876:tc.indicator.count!String."""
        output_var = '#App:9876:tc.indicator.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_date_added_string(self, data):
        """Assert for #App:9876:tc.indicator.date_added!String."""
        output_var = '#App:9876:tc.indicator.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_fail_count_string(self, data):
        """Assert for #App:9876:tc.indicator.fail.count!String."""
        output_var = '#App:9876:tc.indicator.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.indicator.false_positive_count!String."""
        output_var = '#App:9876:tc.indicator.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.indicator.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.indicator.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_id_string(self, data):
        """Assert for #App:9876:tc.indicator.id!String."""
        output_var = '#App:9876:tc.indicator.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_json_raw_string(self, data):
        """Assert for #App:9876:tc.indicator.json.raw!String."""
        output_var = '#App:9876:tc.indicator.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_last_modified_string(self, data):
        """Assert for #App:9876:tc.indicator.last_modified!String."""
        output_var = '#App:9876:tc.indicator.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_last_observed_string(self, data):
        """Assert for #App:9876:tc.indicator.last_observed!String."""
        output_var = '#App:9876:tc.indicator.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_observation_count_string(self, data):
        """Assert for #App:9876:tc.indicator.observation_count!String."""
        output_var = '#App:9876:tc.indicator.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_owner_name_string(self, data):
        """Assert for #App:9876:tc.indicator.owner_name!String."""
        output_var = '#App:9876:tc.indicator.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_private_flag_string(self, data):
        """Assert for #App:9876:tc.indicator.private_flag!String."""
        output_var = '#App:9876:tc.indicator.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_rating_string(self, data):
        """Assert for #App:9876:tc.indicator.rating!String."""
        output_var = '#App:9876:tc.indicator.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.indicator.security_label!StringArray."""
        output_var = '#App:9876:tc.indicator.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_source_string(self, data):
        """Assert for #App:9876:tc.indicator.source!String."""
        output_var = '#App:9876:tc.indicator.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_success_count_string(self, data):
        """Assert for #App:9876:tc.indicator.success.count!String."""
        output_var = '#App:9876:tc.indicator.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_summary_string(self, data):
        """Assert for #App:9876:tc.indicator.summary!String."""
        output_var = '#App:9876:tc.indicator.summary!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_tag_stringarray(self, data):
        """Assert for #App:9876:tc.indicator.tag!StringArray."""
        output_var = '#App:9876:tc.indicator.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_tcentity(self, data):
        """Assert for #App:9876:tc.indicator!TCEntity."""
        output_var = '#App:9876:tc.indicator!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.indicator.threat_assess_score!String."""
        output_var = '#App:9876:tc.indicator.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicator_web_link_string(self, data):
        """Assert for #App:9876:tc.indicator.web_link!String."""
        output_var = '#App:9876:tc.indicator.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_action_string(self, data):
        """Assert for #App:9876:tc.indicators.action!String."""
        output_var = '#App:9876:tc.indicators.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.active_locked!StringArray."""
        output_var = '#App:9876:tc.indicators.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_active_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.active!StringArray."""
        output_var = '#App:9876:tc.indicators.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.confidence!StringArray."""
        output_var = '#App:9876:tc.indicators.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_count_string(self, data):
        """Assert for #App:9876:tc.indicators.count!String."""
        output_var = '#App:9876:tc.indicators.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.date_added!StringArray."""
        output_var = '#App:9876:tc.indicators.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_fail_count_string(self, data):
        """Assert for #App:9876:tc.indicators.fail.count!String."""
        output_var = '#App:9876:tc.indicators.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.indicators.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.indicators.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_id_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.id!StringArray."""
        output_var = '#App:9876:tc.indicators.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_json_raw_string(self, data):
        """Assert for #App:9876:tc.indicators.json.raw!String."""
        output_var = '#App:9876:tc.indicators.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.last_modified!StringArray."""
        output_var = '#App:9876:tc.indicators.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.last_observed!StringArray."""
        output_var = '#App:9876:tc.indicators.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.observation_count!StringArray."""
        output_var = '#App:9876:tc.indicators.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.owner_name!StringArray."""
        output_var = '#App:9876:tc.indicators.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.private_flag!StringArray."""
        output_var = '#App:9876:tc.indicators.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_rating_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.rating!StringArray."""
        output_var = '#App:9876:tc.indicators.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_source_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.source!StringArray."""
        output_var = '#App:9876:tc.indicators.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_success_count_string(self, data):
        """Assert for #App:9876:tc.indicators.success.count!String."""
        output_var = '#App:9876:tc.indicators.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_summary_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.summary!StringArray."""
        output_var = '#App:9876:tc.indicators.summary!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_tcentityarray(self, data):
        """Assert for #App:9876:tc.indicators!TCEntityArray."""
        output_var = '#App:9876:tc.indicators!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.indicators.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_indicators_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.indicators.web_link!StringArray."""
        output_var = '#App:9876:tc.indicators.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_action_string(self, data):
        """Assert for #App:9876:tc.mutex.action!String."""
        output_var = '#App:9876:tc.mutex.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_active_locked_string(self, data):
        """Assert for #App:9876:tc.mutex.active_locked!String."""
        output_var = '#App:9876:tc.mutex.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_active_string(self, data):
        """Assert for #App:9876:tc.mutex.active!String."""
        output_var = '#App:9876:tc.mutex.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.mutex.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.mutex.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_confidence_string(self, data):
        """Assert for #App:9876:tc.mutex.confidence!String."""
        output_var = '#App:9876:tc.mutex.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_count_string(self, data):
        """Assert for #App:9876:tc.mutex.count!String."""
        output_var = '#App:9876:tc.mutex.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_date_added_string(self, data):
        """Assert for #App:9876:tc.mutex.date_added!String."""
        output_var = '#App:9876:tc.mutex.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_fail_count_string(self, data):
        """Assert for #App:9876:tc.mutex.fail.count!String."""
        output_var = '#App:9876:tc.mutex.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.mutex.false_positive_count!String."""
        output_var = '#App:9876:tc.mutex.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.mutex.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.mutex.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_id_string(self, data):
        """Assert for #App:9876:tc.mutex.id!String."""
        output_var = '#App:9876:tc.mutex.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_json_raw_string(self, data):
        """Assert for #App:9876:tc.mutex.json.raw!String."""
        output_var = '#App:9876:tc.mutex.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_last_modified_string(self, data):
        """Assert for #App:9876:tc.mutex.last_modified!String."""
        output_var = '#App:9876:tc.mutex.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_last_observed_string(self, data):
        """Assert for #App:9876:tc.mutex.last_observed!String."""
        output_var = '#App:9876:tc.mutex.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_mutex_string(self, data):
        """Assert for #App:9876:tc.mutex.mutex!String."""
        output_var = '#App:9876:tc.mutex.mutex!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_observation_count_string(self, data):
        """Assert for #App:9876:tc.mutex.observation_count!String."""
        output_var = '#App:9876:tc.mutex.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_owner_name_string(self, data):
        """Assert for #App:9876:tc.mutex.owner_name!String."""
        output_var = '#App:9876:tc.mutex.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_private_flag_string(self, data):
        """Assert for #App:9876:tc.mutex.private_flag!String."""
        output_var = '#App:9876:tc.mutex.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_rating_string(self, data):
        """Assert for #App:9876:tc.mutex.rating!String."""
        output_var = '#App:9876:tc.mutex.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.mutex.security_label!StringArray."""
        output_var = '#App:9876:tc.mutex.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_source_string(self, data):
        """Assert for #App:9876:tc.mutex.source!String."""
        output_var = '#App:9876:tc.mutex.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_success_count_string(self, data):
        """Assert for #App:9876:tc.mutex.success.count!String."""
        output_var = '#App:9876:tc.mutex.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_tag_stringarray(self, data):
        """Assert for #App:9876:tc.mutex.tag!StringArray."""
        output_var = '#App:9876:tc.mutex.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_tcentity(self, data):
        """Assert for #App:9876:tc.mutex!TCEntity."""
        output_var = '#App:9876:tc.mutex!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.mutex.threat_assess_score!String."""
        output_var = '#App:9876:tc.mutex.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutex_web_link_string(self, data):
        """Assert for #App:9876:tc.mutex.web_link!String."""
        output_var = '#App:9876:tc.mutex.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_action_string(self, data):
        """Assert for #App:9876:tc.mutexes.action!String."""
        output_var = '#App:9876:tc.mutexes.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.active_locked!StringArray."""
        output_var = '#App:9876:tc.mutexes.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_active_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.active!StringArray."""
        output_var = '#App:9876:tc.mutexes.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.confidence!StringArray."""
        output_var = '#App:9876:tc.mutexes.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_count_string(self, data):
        """Assert for #App:9876:tc.mutexes.count!String."""
        output_var = '#App:9876:tc.mutexes.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.date_added!StringArray."""
        output_var = '#App:9876:tc.mutexes.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_fail_count_string(self, data):
        """Assert for #App:9876:tc.mutexes.fail.count!String."""
        output_var = '#App:9876:tc.mutexes.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.mutexes.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.mutexes.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_id_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.id!StringArray."""
        output_var = '#App:9876:tc.mutexes.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_json_raw_string(self, data):
        """Assert for #App:9876:tc.mutexes.json.raw!String."""
        output_var = '#App:9876:tc.mutexes.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.last_modified!StringArray."""
        output_var = '#App:9876:tc.mutexes.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.last_observed!StringArray."""
        output_var = '#App:9876:tc.mutexes.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_mutex_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.mutex!StringArray."""
        output_var = '#App:9876:tc.mutexes.mutex!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.observation_count!StringArray."""
        output_var = '#App:9876:tc.mutexes.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.owner_name!StringArray."""
        output_var = '#App:9876:tc.mutexes.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.private_flag!StringArray."""
        output_var = '#App:9876:tc.mutexes.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_rating_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.rating!StringArray."""
        output_var = '#App:9876:tc.mutexes.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_source_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.source!StringArray."""
        output_var = '#App:9876:tc.mutexes.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_success_count_string(self, data):
        """Assert for #App:9876:tc.mutexes.success.count!String."""
        output_var = '#App:9876:tc.mutexes.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_tcentityarray(self, data):
        """Assert for #App:9876:tc.mutexes!TCEntityArray."""
        output_var = '#App:9876:tc.mutexes!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.mutexes.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_mutexes_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.mutexes.web_link!StringArray."""
        output_var = '#App:9876:tc.mutexes.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_action_string(self, data):
        """Assert for #App:9876:tc.registry_key.action!String."""
        output_var = '#App:9876:tc.registry_key.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_active_locked_string(self, data):
        """Assert for #App:9876:tc.registry_key.active_locked!String."""
        output_var = '#App:9876:tc.registry_key.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_active_string(self, data):
        """Assert for #App:9876:tc.registry_key.active!String."""
        output_var = '#App:9876:tc.registry_key.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.registry_key.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.registry_key.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_confidence_string(self, data):
        """Assert for #App:9876:tc.registry_key.confidence!String."""
        output_var = '#App:9876:tc.registry_key.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_count_string(self, data):
        """Assert for #App:9876:tc.registry_key.count!String."""
        output_var = '#App:9876:tc.registry_key.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_date_added_string(self, data):
        """Assert for #App:9876:tc.registry_key.date_added!String."""
        output_var = '#App:9876:tc.registry_key.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_fail_count_string(self, data):
        """Assert for #App:9876:tc.registry_key.fail.count!String."""
        output_var = '#App:9876:tc.registry_key.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.registry_key.false_positive_count!String."""
        output_var = '#App:9876:tc.registry_key.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.registry_key.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.registry_key.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_id_string(self, data):
        """Assert for #App:9876:tc.registry_key.id!String."""
        output_var = '#App:9876:tc.registry_key.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_json_raw_string(self, data):
        """Assert for #App:9876:tc.registry_key.json.raw!String."""
        output_var = '#App:9876:tc.registry_key.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_key_name_string(self, data):
        """Assert for #App:9876:tc.registry_key.key_name!String."""
        output_var = '#App:9876:tc.registry_key.key_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_last_modified_string(self, data):
        """Assert for #App:9876:tc.registry_key.last_modified!String."""
        output_var = '#App:9876:tc.registry_key.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_last_observed_string(self, data):
        """Assert for #App:9876:tc.registry_key.last_observed!String."""
        output_var = '#App:9876:tc.registry_key.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_observation_count_string(self, data):
        """Assert for #App:9876:tc.registry_key.observation_count!String."""
        output_var = '#App:9876:tc.registry_key.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_owner_name_string(self, data):
        """Assert for #App:9876:tc.registry_key.owner_name!String."""
        output_var = '#App:9876:tc.registry_key.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_private_flag_string(self, data):
        """Assert for #App:9876:tc.registry_key.private_flag!String."""
        output_var = '#App:9876:tc.registry_key.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_rating_string(self, data):
        """Assert for #App:9876:tc.registry_key.rating!String."""
        output_var = '#App:9876:tc.registry_key.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.registry_key.security_label!StringArray."""
        output_var = '#App:9876:tc.registry_key.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_source_string(self, data):
        """Assert for #App:9876:tc.registry_key.source!String."""
        output_var = '#App:9876:tc.registry_key.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_success_count_string(self, data):
        """Assert for #App:9876:tc.registry_key.success.count!String."""
        output_var = '#App:9876:tc.registry_key.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_tag_stringarray(self, data):
        """Assert for #App:9876:tc.registry_key.tag!StringArray."""
        output_var = '#App:9876:tc.registry_key.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_tcentity(self, data):
        """Assert for #App:9876:tc.registry_key!TCEntity."""
        output_var = '#App:9876:tc.registry_key!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.registry_key.threat_assess_score!String."""
        output_var = '#App:9876:tc.registry_key.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_value_name_string(self, data):
        """Assert for #App:9876:tc.registry_key.value_name!String."""
        output_var = '#App:9876:tc.registry_key.value_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_value_type_string(self, data):
        """Assert for #App:9876:tc.registry_key.value_type!String."""
        output_var = '#App:9876:tc.registry_key.value_type!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_key_web_link_string(self, data):
        """Assert for #App:9876:tc.registry_key.web_link!String."""
        output_var = '#App:9876:tc.registry_key.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_action_string(self, data):
        """Assert for #App:9876:tc.registry_keys.action!String."""
        output_var = '#App:9876:tc.registry_keys.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.active_locked!StringArray."""
        output_var = '#App:9876:tc.registry_keys.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_active_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.active!StringArray."""
        output_var = '#App:9876:tc.registry_keys.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.confidence!StringArray."""
        output_var = '#App:9876:tc.registry_keys.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_count_string(self, data):
        """Assert for #App:9876:tc.registry_keys.count!String."""
        output_var = '#App:9876:tc.registry_keys.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.date_added!StringArray."""
        output_var = '#App:9876:tc.registry_keys.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_fail_count_string(self, data):
        """Assert for #App:9876:tc.registry_keys.fail.count!String."""
        output_var = '#App:9876:tc.registry_keys.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.registry_keys.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.registry_keys.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_id_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.id!StringArray."""
        output_var = '#App:9876:tc.registry_keys.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_json_raw_string(self, data):
        """Assert for #App:9876:tc.registry_keys.json.raw!String."""
        output_var = '#App:9876:tc.registry_keys.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_key_name_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.key_name!StringArray."""
        output_var = '#App:9876:tc.registry_keys.key_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.last_modified!StringArray."""
        output_var = '#App:9876:tc.registry_keys.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.last_observed!StringArray."""
        output_var = '#App:9876:tc.registry_keys.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.observation_count!StringArray."""
        output_var = '#App:9876:tc.registry_keys.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.owner_name!StringArray."""
        output_var = '#App:9876:tc.registry_keys.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.private_flag!StringArray."""
        output_var = '#App:9876:tc.registry_keys.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_rating_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.rating!StringArray."""
        output_var = '#App:9876:tc.registry_keys.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_source_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.source!StringArray."""
        output_var = '#App:9876:tc.registry_keys.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_success_count_string(self, data):
        """Assert for #App:9876:tc.registry_keys.success.count!String."""
        output_var = '#App:9876:tc.registry_keys.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_tcentityarray(self, data):
        """Assert for #App:9876:tc.registry_keys!TCEntityArray."""
        output_var = '#App:9876:tc.registry_keys!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.registry_keys.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_value_name_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.value_name!StringArray."""
        output_var = '#App:9876:tc.registry_keys.value_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_value_type_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.value_type!StringArray."""
        output_var = '#App:9876:tc.registry_keys.value_type!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_registry_keys_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.registry_keys.web_link!StringArray."""
        output_var = '#App:9876:tc.registry_keys.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_action_string(self, data):
        """Assert for #App:9876:tc.url.action!String."""
        output_var = '#App:9876:tc.url.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_active_locked_string(self, data):
        """Assert for #App:9876:tc.url.active_locked!String."""
        output_var = '#App:9876:tc.url.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_active_string(self, data):
        """Assert for #App:9876:tc.url.active!String."""
        output_var = '#App:9876:tc.url.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.url.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.url.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_confidence_string(self, data):
        """Assert for #App:9876:tc.url.confidence!String."""
        output_var = '#App:9876:tc.url.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_count_string(self, data):
        """Assert for #App:9876:tc.url.count!String."""
        output_var = '#App:9876:tc.url.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_date_added_string(self, data):
        """Assert for #App:9876:tc.url.date_added!String."""
        output_var = '#App:9876:tc.url.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_fail_count_string(self, data):
        """Assert for #App:9876:tc.url.fail.count!String."""
        output_var = '#App:9876:tc.url.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.url.false_positive_count!String."""
        output_var = '#App:9876:tc.url.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.url.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.url.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_id_string(self, data):
        """Assert for #App:9876:tc.url.id!String."""
        output_var = '#App:9876:tc.url.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_json_raw_string(self, data):
        """Assert for #App:9876:tc.url.json.raw!String."""
        output_var = '#App:9876:tc.url.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_last_modified_string(self, data):
        """Assert for #App:9876:tc.url.last_modified!String."""
        output_var = '#App:9876:tc.url.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_last_observed_string(self, data):
        """Assert for #App:9876:tc.url.last_observed!String."""
        output_var = '#App:9876:tc.url.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_observation_count_string(self, data):
        """Assert for #App:9876:tc.url.observation_count!String."""
        output_var = '#App:9876:tc.url.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_owner_name_string(self, data):
        """Assert for #App:9876:tc.url.owner_name!String."""
        output_var = '#App:9876:tc.url.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_private_flag_string(self, data):
        """Assert for #App:9876:tc.url.private_flag!String."""
        output_var = '#App:9876:tc.url.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_rating_string(self, data):
        """Assert for #App:9876:tc.url.rating!String."""
        output_var = '#App:9876:tc.url.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.url.security_label!StringArray."""
        output_var = '#App:9876:tc.url.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_source_string(self, data):
        """Assert for #App:9876:tc.url.source!String."""
        output_var = '#App:9876:tc.url.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_success_count_string(self, data):
        """Assert for #App:9876:tc.url.success.count!String."""
        output_var = '#App:9876:tc.url.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_tag_stringarray(self, data):
        """Assert for #App:9876:tc.url.tag!StringArray."""
        output_var = '#App:9876:tc.url.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_tcentity(self, data):
        """Assert for #App:9876:tc.url!TCEntity."""
        output_var = '#App:9876:tc.url!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_text_string(self, data):
        """Assert for #App:9876:tc.url.text!String."""
        output_var = '#App:9876:tc.url.text!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.url.threat_assess_score!String."""
        output_var = '#App:9876:tc.url.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_url_web_link_string(self, data):
        """Assert for #App:9876:tc.url.web_link!String."""
        output_var = '#App:9876:tc.url.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_action_string(self, data):
        """Assert for #App:9876:tc.urls.action!String."""
        output_var = '#App:9876:tc.urls.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.urls.active_locked!StringArray."""
        output_var = '#App:9876:tc.urls.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_active_stringarray(self, data):
        """Assert for #App:9876:tc.urls.active!StringArray."""
        output_var = '#App:9876:tc.urls.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.urls.confidence!StringArray."""
        output_var = '#App:9876:tc.urls.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_count_string(self, data):
        """Assert for #App:9876:tc.urls.count!String."""
        output_var = '#App:9876:tc.urls.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.urls.date_added!StringArray."""
        output_var = '#App:9876:tc.urls.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_fail_count_string(self, data):
        """Assert for #App:9876:tc.urls.fail.count!String."""
        output_var = '#App:9876:tc.urls.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.urls.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.urls.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.urls.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.urls.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_id_stringarray(self, data):
        """Assert for #App:9876:tc.urls.id!StringArray."""
        output_var = '#App:9876:tc.urls.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_json_raw_string(self, data):
        """Assert for #App:9876:tc.urls.json.raw!String."""
        output_var = '#App:9876:tc.urls.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.urls.last_modified!StringArray."""
        output_var = '#App:9876:tc.urls.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.urls.last_observed!StringArray."""
        output_var = '#App:9876:tc.urls.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.urls.observation_count!StringArray."""
        output_var = '#App:9876:tc.urls.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.urls.owner_name!StringArray."""
        output_var = '#App:9876:tc.urls.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.urls.private_flag!StringArray."""
        output_var = '#App:9876:tc.urls.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_rating_stringarray(self, data):
        """Assert for #App:9876:tc.urls.rating!StringArray."""
        output_var = '#App:9876:tc.urls.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_source_stringarray(self, data):
        """Assert for #App:9876:tc.urls.source!StringArray."""
        output_var = '#App:9876:tc.urls.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_success_count_string(self, data):
        """Assert for #App:9876:tc.urls.success.count!String."""
        output_var = '#App:9876:tc.urls.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_tcentityarray(self, data):
        """Assert for #App:9876:tc.urls!TCEntityArray."""
        output_var = '#App:9876:tc.urls!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_text_stringarray(self, data):
        """Assert for #App:9876:tc.urls.text!StringArray."""
        output_var = '#App:9876:tc.urls.text!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.urls.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.urls.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_urls_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.urls.web_link!StringArray."""
        output_var = '#App:9876:tc.urls.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_action_string(self, data):
        """Assert for #App:9876:tc.user_agent.action!String."""
        output_var = '#App:9876:tc.user_agent.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_active_locked_string(self, data):
        """Assert for #App:9876:tc.user_agent.active_locked!String."""
        output_var = '#App:9876:tc.user_agent.active_locked!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_active_string(self, data):
        """Assert for #App:9876:tc.user_agent.active!String."""
        output_var = '#App:9876:tc.user_agent.active!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_attribute_tcentityarray(self, data):
        """Assert for #App:9876:tc.user_agent.attribute!TCEntityArray."""
        output_var = '#App:9876:tc.user_agent.attribute!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_confidence_string(self, data):
        """Assert for #App:9876:tc.user_agent.confidence!String."""
        output_var = '#App:9876:tc.user_agent.confidence!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_count_string(self, data):
        """Assert for #App:9876:tc.user_agent.count!String."""
        output_var = '#App:9876:tc.user_agent.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_date_added_string(self, data):
        """Assert for #App:9876:tc.user_agent.date_added!String."""
        output_var = '#App:9876:tc.user_agent.date_added!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_fail_count_string(self, data):
        """Assert for #App:9876:tc.user_agent.fail.count!String."""
        output_var = '#App:9876:tc.user_agent.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_false_positive_count_string(self, data):
        """Assert for #App:9876:tc.user_agent.false_positive_count!String."""
        output_var = '#App:9876:tc.user_agent.false_positive_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_false_positive_last_reported_string(self, data):
        """Assert for #App:9876:tc.user_agent.false_positive_last_reported!String."""
        output_var = '#App:9876:tc.user_agent.false_positive_last_reported!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_id_string(self, data):
        """Assert for #App:9876:tc.user_agent.id!String."""
        output_var = '#App:9876:tc.user_agent.id!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_json_raw_string(self, data):
        """Assert for #App:9876:tc.user_agent.json.raw!String."""
        output_var = '#App:9876:tc.user_agent.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_last_modified_string(self, data):
        """Assert for #App:9876:tc.user_agent.last_modified!String."""
        output_var = '#App:9876:tc.user_agent.last_modified!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_last_observed_string(self, data):
        """Assert for #App:9876:tc.user_agent.last_observed!String."""
        output_var = '#App:9876:tc.user_agent.last_observed!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_observation_count_string(self, data):
        """Assert for #App:9876:tc.user_agent.observation_count!String."""
        output_var = '#App:9876:tc.user_agent.observation_count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_owner_name_string(self, data):
        """Assert for #App:9876:tc.user_agent.owner_name!String."""
        output_var = '#App:9876:tc.user_agent.owner_name!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_private_flag_string(self, data):
        """Assert for #App:9876:tc.user_agent.private_flag!String."""
        output_var = '#App:9876:tc.user_agent.private_flag!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_rating_string(self, data):
        """Assert for #App:9876:tc.user_agent.rating!String."""
        output_var = '#App:9876:tc.user_agent.rating!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_security_label_stringarray(self, data):
        """Assert for #App:9876:tc.user_agent.security_label!StringArray."""
        output_var = '#App:9876:tc.user_agent.security_label!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_source_string(self, data):
        """Assert for #App:9876:tc.user_agent.source!String."""
        output_var = '#App:9876:tc.user_agent.source!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_success_count_string(self, data):
        """Assert for #App:9876:tc.user_agent.success.count!String."""
        output_var = '#App:9876:tc.user_agent.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_tag_stringarray(self, data):
        """Assert for #App:9876:tc.user_agent.tag!StringArray."""
        output_var = '#App:9876:tc.user_agent.tag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_tcentity(self, data):
        """Assert for #App:9876:tc.user_agent!TCEntity."""
        output_var = '#App:9876:tc.user_agent!TCEntity'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_threat_assess_score_string(self, data):
        """Assert for #App:9876:tc.user_agent.threat_assess_score!String."""
        output_var = '#App:9876:tc.user_agent.threat_assess_score!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_user_agent_string_string(self, data):
        """Assert for #App:9876:tc.user_agent.user_agent_string!String."""
        output_var = '#App:9876:tc.user_agent.user_agent_string!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agent_web_link_string(self, data):
        """Assert for #App:9876:tc.user_agent.web_link!String."""
        output_var = '#App:9876:tc.user_agent.web_link!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_action_string(self, data):
        """Assert for #App:9876:tc.user_agents.action!String."""
        output_var = '#App:9876:tc.user_agents.action!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_active_locked_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.active_locked!StringArray."""
        output_var = '#App:9876:tc.user_agents.active_locked!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_active_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.active!StringArray."""
        output_var = '#App:9876:tc.user_agents.active!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_confidence_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.confidence!StringArray."""
        output_var = '#App:9876:tc.user_agents.confidence!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_count_string(self, data):
        """Assert for #App:9876:tc.user_agents.count!String."""
        output_var = '#App:9876:tc.user_agents.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_date_added_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.date_added!StringArray."""
        output_var = '#App:9876:tc.user_agents.date_added!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_fail_count_string(self, data):
        """Assert for #App:9876:tc.user_agents.fail.count!String."""
        output_var = '#App:9876:tc.user_agents.fail.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_false_positive_count_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.false_positive_count!StringArray."""
        output_var = '#App:9876:tc.user_agents.false_positive_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_false_positive_last_reported_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.false_positive_last_reported!StringArray."""
        output_var = '#App:9876:tc.user_agents.false_positive_last_reported!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_id_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.id!StringArray."""
        output_var = '#App:9876:tc.user_agents.id!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_json_raw_string(self, data):
        """Assert for #App:9876:tc.user_agents.json.raw!String."""
        output_var = '#App:9876:tc.user_agents.json.raw!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_last_modified_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.last_modified!StringArray."""
        output_var = '#App:9876:tc.user_agents.last_modified!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_last_observed_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.last_observed!StringArray."""
        output_var = '#App:9876:tc.user_agents.last_observed!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_observation_count_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.observation_count!StringArray."""
        output_var = '#App:9876:tc.user_agents.observation_count!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_owner_name_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.owner_name!StringArray."""
        output_var = '#App:9876:tc.user_agents.owner_name!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_private_flag_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.private_flag!StringArray."""
        output_var = '#App:9876:tc.user_agents.private_flag!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_rating_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.rating!StringArray."""
        output_var = '#App:9876:tc.user_agents.rating!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_source_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.source!StringArray."""
        output_var = '#App:9876:tc.user_agents.source!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_success_count_string(self, data):
        """Assert for #App:9876:tc.user_agents.success.count!String."""
        output_var = '#App:9876:tc.user_agents.success.count!String'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_tcentityarray(self, data):
        """Assert for #App:9876:tc.user_agents!TCEntityArray."""
        output_var = '#App:9876:tc.user_agents!TCEntityArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_threat_assess_score_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.threat_assess_score!StringArray."""
        output_var = '#App:9876:tc.user_agents.threat_assess_score!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_user_agent_string_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.user_agent_string!StringArray."""
        output_var = '#App:9876:tc.user_agents.user_agent_string!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error

    def tc_user_agents_web_link_stringarray(self, data):
        """Assert for #App:9876:tc.user_agents.web_link!StringArray."""
        output_var = '#App:9876:tc.user_agents.web_link!StringArray'
        passed, assert_error = self.validator.redis.data(
            output_var, data.pop('expected_output'), data.pop('op', '='), **data
        )
        assert passed, assert_error
