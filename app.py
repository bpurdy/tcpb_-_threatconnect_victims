# -*- coding: utf-8 -*-
"""ThreatConnect Playbook App"""

import json
from urllib.parse import unquote
from requests import Response

from playbook_app import PlaybookApp


class App(PlaybookApp):
    """Indicator Playbook App"""

    def __init__(self, _tcex):
        super(App, self).__init__(_tcex)
        self.responses = []
        self.tc_action = self.tcex.args.tc_action.lower()
        self.indicator_type = self.tcex.playbook.read_choice(
            self.args.indicator_type_1, self.args.indicator_type_input
        )
        if self.tc_action in ['update']:
            self.values = self.tcex.playbook.read(self.args.indicator_entity_or_value) or {}
            if isinstance(self.values, dict):
                self.values = self.values.get('value', '')
            self.values = [self.values]
        else:
            self.values = self._consolidate_arg('indicator_value', 'indicator_values')
        self.fetch_by = self.tcex.playbook.read(self.args.get_by) or ''
        if self.tc_action == 'delete':
            self.fetch_by = self.tcex.playbook.read(self.args.delete_by)
        self.fetch_by = self.fetch_by.lower()
        if self.fetch_by == 'entity':
            indicator_entity = self.tcex.playbook.read(self.args.indicator_entity) or {}
            self.values = [indicator_entity.get('value')]
            self.indicator_type = indicator_entity.get('type') or ''
        elif self.fetch_by == 'association':
            indicator_entity = self.tcex.playbook.read(self.args.associated_to) or {}
            self.values = [indicator_entity.get('value')]
            self.indicator_type = indicator_entity.get('type') or ''
        if self.tc_action in ['get', 'delete'] and self.fetch_by not in [
            'id',
            'entity',
            'association',
            'value',
        ]:
            self.indicator_type = self.tcex.playbook.read_choice(
                self.args.indicator_type, self.args.indicator_type_input
            )
        self.indicator_tag = self.tcex.playbook.read(self.args.indicator_tag)
        self.owner = self.tcex.playbook.read_choice(self.args.owner, self.args.owner_input)

        self.custom_params = {}
        if self.is_custom(self.indicator_type):
            custom_indicator = self.tcex.indicator_types_data.get(self.indicator_type)
            label_1 = custom_indicator.get('value1Label', '')
            label_2 = custom_indicator.get('value2Label', '')
            label_3 = custom_indicator.get('value3Label', '')
            if self.tc_action in ['create']:
                self.custom_params = {
                    label_1: self.tcex.playbook.read(self.args.custom_value_1, True),
                    label_2: self.tcex.playbook.read(self.args.custom_value_2, True),
                    label_3: self.tcex.playbook.read(self.args.custom_value_3, True),
                }
            elif self.tc_action in ['create multiple']:
                self.custom_params = {
                    label_1: self._read_array('custom_values_1'),
                    label_2: self._read_array('custom_values_2'),
                    label_3: self._read_array('custom_values_3'),
                }

        # metadata
        self.associated_to = self.tcex.playbook.read(self.args.associated_to)
        self.rating = self.tcex.playbook.read(self.args.rating)
        self.confidence = self.tcex.playbook.read(self.args.confidence)

        # flags
        self.fail_on_error = self.args.fail_on_error
        self.fail_on_no_results = self.args.fail_on_no_results

        # Registry Key
        self.resource_name = self._consolidate_arg('resource_name', 'resource_names')
        self.value_name = self._consolidate_arg('value_name', 'value_names')
        self.value_type = self._consolidate_arg('value_type', 'value_types')

        # Host
        self.dns_active = self._consolidate_arg('dns_active', 'dns_active', True)
        self.whois_active = self._consolidate_arg('whois_active', 'whois_active', True)

        # File
        self.md5 = self._consolidate_arg('md5_hash', 'md5_hashes')
        self.sha1 = self._consolidate_arg('sha1_hash', 'sha1_hashes')
        self.sha256 = self._consolidate_arg('sha256_hash', 'sha256_hashes')
        self.file_size = self._consolidate_arg('file_size', 'file_sizes')

        self._ti_associated_to = None

        if self.indicator_type:
            self.indicator_type = self.indicator_type.lower()
        if self.indicator_type == 'indicator':
            self.indicator_type = None

    def _fetch(self):
        """Fetches the indicator(s) for 'Get' or 'Delete' action"""
        indicators = []
        if self.fetch_by in ['value', 'entity']:
            for value in list(filter(None, self.values)):
                value = value.replace('/', '%2F') if self.tc_action in ['delete'] else value
                indicators.append(
                    self.tcex.ti.indicator(
                        unique_id=value, indicator_type=self.indicator_type, owner=self.owner
                    )
                )
        elif self.fetch_by in ['tag', 'association']:
            iterator = []
            if self.fetch_by in ['association']:
                association = self._transform_to_ti_associations(self.associated_to)
                if association:
                    iterator = association.indicator_associations_types(self.indicator_type)
            else:
                tag = self.tcex.ti.tag(self.indicator_tag)
                iterator = tag.indicators(indicator_type=self.indicator_type, owner=self.owner)
            for indicator in iterator:
                indicator['owner'] = indicator.pop('ownerName') or self.owner
                if indicator.get('summary'):
                    indicator['unique_id'] = indicator.pop('summary')
                object_type = indicator.pop('type', self.indicator_type)
                indicators.append(self.indicator_or_group(object_type, indicator))
        return indicators

    @staticmethod
    def is_custom(indicator_type):
        """Determines if indicator_type is a custom type"""
        if not indicator_type:
            return False
        indicator_type = indicator_type.lower()
        if indicator_type in [
            'indicator',
            'email address',
            'email subject',
            'hashtag',
            'host',
            'file',
            'mutex',
            'registry key',
            'url',
            'user agent',
            'asn',
            'cidr',
            'address',
        ]:
            return False
        return True

    def check_response(self, response):
        """Handles general response errors"""
        if isinstance(response, RuntimeError):
            self.tcex.log.error(f'Exception in response: {str(response)}')
            return {
                'status': response.args[0],
                'message': response.args[1],
            }
        if isinstance(response, Response) and not response.ok:
            if (
                self.tc_action in ['get', 'delete']
                and self.fail_on_no_results
                and response.status_code in [404, 405]
            ):
                self.tcex.playbook.exit(1, 'No Results')
            elif self.tc_action not in ['get', 'delete'] and self.fail_on_error:  # pragma: no cover
                self.tcex.playbook.exit(1, response.json().get('message', response.reason))
        return None

    @property
    def base_key(self):
        """Returns the base key to use when constructing output parameters"""
        indicator = self.tcex.ti.indicator(indicator_type=self.indicator_type)
        if self._output_single():
            output_base_key = indicator.api_entity
            if self.fetch_by in ['entity']:
                output_base_key = 'indicator'
            elif self.is_custom(self.indicator_type):
                output_base_key = 'custom_indicator'
        else:
            output_base_key = (indicator.api_branch or indicator.api_type).replace(' ', '_')
            if self.fetch_by in ['association']:
                output_base_key = 'indicators'
            elif self.is_custom(self.indicator_type):
                output_base_key = 'custom_indicators'
        output_base_key = f'tc.{self.tcex.utils.camel_to_snake(output_base_key)}'

        return output_base_key

    def single_or_append(self, outputs, key, value):
        """Assigns either a single value or appends the value to a array for the given key."""
        if self._output_single():
            outputs[key] = value
        else:
            outputs.setdefault(key, []).append(value)

    def write_output(self):
        """
        Write the Playbook output variables.

        This method should be overridden with the output variables defined in the install.json
        configuration file.
        """

        output_base_key = self.base_key
        success_count = 0
        failure_count = 0
        outputs = {}

        for indicator_type, response in self.responses:
            try:
                error_message = self.check_response(response)
                if error_message:
                    failure_count += 1
                    outputs[f'{output_base_key}.json.raw'] = error_message
                    continue

                self.single_or_append(outputs, f'{output_base_key}.json.raw', response.json())

                if self.tc_action == 'delete' and response.ok:
                    success_count += 1
                    continue

                response = response.json()
                indicator = self.indicator_or_group(indicator_type)
                data = response.get('data', {}).get(indicator.api_entity)
                indicator = self.indicator_or_group(indicator_type, data)
                for index, param in enumerate(self.custom_params, start=1):
                    self.single_or_append(
                        outputs, f'{output_base_key}.value_{index}', data.get(param)
                    )
                self.single_or_append(outputs, f'{output_base_key}', indicator.as_entity)
                self.single_or_append(
                    outputs, f'{output_base_key}.summary', unquote(indicator.unique_id)
                )
                self.single_or_append(
                    outputs, f'{output_base_key}.owner_name', data.get('owner', {}).get('name')
                )
                if self._output_single() and self.tc_action in ['get']:
                    for attribute in data.get('attribute', []):
                        outputs.setdefault(f'{output_base_key}.attribute', []).append(
                            {
                                'value': attribute.get('value'),
                                'type': attribute.get('type'),
                                'id': attribute.get('id', ''),
                            }
                        )
                    outputs[f'{output_base_key}.tag'] = [
                        tag.get('name') for tag in data.get('tag', [])
                    ]
                    outputs[f'{output_base_key}.security_label'] = [
                        label.get('name') for label in data.get('securityLabel', [])
                    ]

                self._outputs_from_response(
                    data,
                    output_base_key,
                    outputs,
                    exclude=[
                        'attribute',
                        'tag',
                        'security_label',
                        'owner',
                        'threat_assess_confidence',
                        'threat_assess_rating',
                    ],
                    as_array=not self._output_single(),
                )
                success_count += 1
            except Exception:
                failure_count += 1

        outputs[f'{output_base_key}.success.count'] = success_count
        outputs[f'{output_base_key}.fail.count'] = failure_count
        outputs[f'{output_base_key}.count'] = failure_count + success_count
        outputs[f'{output_base_key}.action'] = self.tc_action

        self._handle_errs(outputs.get(f'{output_base_key}.json.raw', []), failure_count)

        for key, value in outputs.items():
            if isinstance(value, list) and all(v is None for v in value):
                continue
            if key.endswith('.json.raw'):
                value = json.dumps(value)
            self.tcex.playbook.create_output(key, value)

    def _handle_errs(self, errs, failure_count):
        """ handle outputting a nice msg """
        if not self.fail_on_no_results and not self.fail_on_error:
            return

        if not isinstance(errs, list):
            errs = [errs]

        msgs = []
        for e in errs:
            # we only handle 905/910/915/920
            if e.get('status') in [905, 910, 915, 920]:  # pragma: no cover
                msgs.append(e.get('message'))
            else:
                msgs.append(e)
        if not self.responses and self.fail_on_no_results:
            self.tcex.playbook.exit(1, 'No Results')
        if failure_count > 0 and self.fail_on_error and msgs:  # pragma: no cover
            output_msgs = ''.join(str(elem) for elem in msgs)
            self.tcex.log.debug(f'Error msg: {output_msgs}')
            self.tcex.playbook.exit(1, output_msgs)

    def indicator_or_group(self, object_type, data=None):
        """Returns either a Group or Indicator for a given object type"""
        if data is None:
            data = {}
        try:
            return self.tcex.ti.indicator(indicator_type=object_type, **data)
        except RuntimeError:
            return self.tcex.ti.group(group_type=object_type, **data)

    def _transform_to_ti_associations(self, assoc):
        """ Convert passed in associations to TI objects representing associations """

        if not isinstance(assoc, dict):
            return None

        kwargs = {'owner': self.owner, 'unique_id': assoc.get('id')}
        association_type = assoc.get('type')
        if association_type in self.tcex.indicator_types:
            kwargs['unique_id'] = assoc.get('value')
            return self.tcex.ti.indicator(indicator_type=association_type, **kwargs)
        if association_type in self.tcex.group_types:
            return self.tcex.ti.group(group_type=association_type, **kwargs)
        if association_type.lower() in ['victimassets']:
            return self.tcex.ti.victim(**kwargs)
        return None

    def _output_single(self):
        """Returns true if action is used to return single result, otherwise returns false"""
        if (
            self.tc_action in ['create', 'update']
            or (self.tc_action == 'get' and self.fetch_by in ['value', 'entity'])
            or (self.tc_action == 'delete' and self.fetch_by in ['value', 'entity'])
        ):
            return True
        return False

    def _outputs_from_response(
        self, response, base_key, current_outputs, as_array=False, exclude=None
    ):
        """Constructs output params given a dict"""
        if exclude is None:  # pragma: no cover
            exclude = []
        for key, value in response.items():
            key = (
                'as_number'
                if key in ['AS Number']
                else self.tcex.utils.camel_to_snake(key.replace(' ', ''))
            )
            if key in exclude:
                continue
            if value is not None and not isinstance(value, str):
                value = json.dumps(value)
            if as_array:
                current_outputs.setdefault(f'{base_key}.{key}', []).append(value)
            else:
                current_outputs[f'{base_key}.{key}'] = value

    def build_kwargs(self):
        """Builds the kwargs and iterates over them."""
        key_value_pairs = []

        if self.indicator_type in ['address']:
            key_value_pairs.append({'key': 'ip', 'value': self.values})
        if self.indicator_type in ['email subject']:
            key_value_pairs.append({'key': 'Subject', 'value': self.values})
        if self.indicator_type in ['hashtag']:
            key_value_pairs.append({'key': 'Hashtag', 'value': self.values})
        elif self.indicator_type in ['email address']:
            key_value_pairs.append({'key': 'address', 'value': self.values})
        elif self.indicator_type in ['url']:
            key_value_pairs.append({'key': 'text', 'value': self.values})
        elif self.indicator_type in ['host']:
            key_value_pairs.append({'key': 'hostname', 'value': self.values})
            key_value_pairs.append({'key': 'dns_active', 'value': self.dns_active})
            key_value_pairs.append({'key': 'whois_active', 'value': self.whois_active})
        elif self.indicator_type in ['file']:  # pragma: no cover
            key_value_pairs.append({'key': 'md5', 'value': self.md5})
            key_value_pairs.append({'key': 'sha1', 'value': self.sha1})
            key_value_pairs.append({'key': 'sha256', 'value': self.sha256})
        elif self.indicator_type in ['asn']:
            key_value_pairs.append({'key': 'AS Number', 'value': self.values})
        elif self.indicator_type in ['cidr']:
            key_value_pairs.append({'key': 'Block', 'value': self.values})
        elif self.indicator_type in ['mutex']:
            key_value_pairs.append({'key': 'Mutex', 'value': self.values})
        elif self.indicator_type in ['registry key']:
            key_value_pairs.append({'key': 'Key Name', 'value': self.resource_name})
            key_value_pairs.append({'key': 'Value Name', 'value': self.value_name})
            key_value_pairs.append({'key': 'Value Type', 'value': self.value_type})
        elif self.indicator_type in ['user agent']:
            key_value_pairs.append({'key': 'User Agent String', 'value': self.values})
        else:
            for key, value in self.custom_params.items():
                key_value_pairs.append({'key': key, 'value': value})

        length = 0
        for pair in key_value_pairs:
            if pair.get('value') and isinstance(pair.get('value'), list):
                length = len(pair.get('value'))

        for pair in key_value_pairs:
            if len(pair.get('value', [])) != length:
                self.tcex.log.error(f'ERROR: invalid {pair.get("key")} length. ')
                self.tcex.playbook.exit(1, 'Invalid argument lengths')

        kwargs = {'owner': self.owner, 'indicator_type': self.indicator_type}
        if self.tc_action == 'update' and self.rating:
            kwargs['rating'] = self.rating
        if self.tc_action == 'update' and self.confidence:
            kwargs['confidence'] = self.confidence
        for i in range(length):  # pylint: disable=unused-variable
            for pair in key_value_pairs:
                kwargs[pair.get('key')] = pair.get('value').pop(0)
            yield kwargs

    def create(self):
        """Handles the create action."""
        try:
            for kwarg in self.build_kwargs():
                indicator = self.tcex.ti.indicator(**kwarg)
                self.responses.append((indicator.api_sub_type, indicator.create()))
        except RuntimeError as e:  # pragma: no cover
            if not (isinstance(e, RuntimeError) and e.args[0] == 950 and self.fail_on_no_results):
                self.responses.append((self.indicator_type, e))

    def create_multiple(self):
        """Handles the create multiple action."""
        self.create()

    def get(self):
        """Handles the get action."""
        parameters = {'includes': ['additional', 'attributes', 'labels', 'tags']}
        try:
            for indicator in self._fetch():
                self.responses.append((indicator.api_sub_type, indicator.single(params=parameters)))
        except RuntimeError as e:
            if not (isinstance(e, RuntimeError) and e.args[0] == 950 and self.fail_on_no_results):
                self.responses.append((self.indicator_type, e))

    def update(self):
        """Handles the update action."""
        try:
            for kwarg in self.build_kwargs():
                indicator = self.tcex.ti.indicator(**kwarg)
                self.responses.append((indicator.api_sub_type, indicator.update()))
        except RuntimeError as e:  # pragma: no cover
            if not (isinstance(e, RuntimeError) and e.args[0] == 950 and self.fail_on_no_results):
                self.responses.append((self.indicator_type, e))

    def delete(self):
        """Handles the delete action."""
        try:
            for indicator in self._fetch():
                self.responses.append((indicator.api_sub_type, indicator.delete()))
        except RuntimeError as e:
            if not (isinstance(e, RuntimeError) and e.args[0] == 950 and self.fail_on_no_results):
                self.responses.append((self.indicator_type, e))

    def _consolidate_arg(self, individual, multiple, apply=False):
        """Consolidates a argument into a more manageable subset."""
        length = len(self._read_array('indicator_values'))
        if self.tc_action != 'create multiple':
            length = 1
            apply = True

        individual = getattr(self.args, individual)
        multiple = self._read_array(multiple)

        return_value = multiple
        if apply:
            return_value = [individual] * length

        return return_value

    def _read_array(self, arg_name: str):
        """Converts a string or list of lists to a single list"""
        arg = getattr(self.args, arg_name)
        if not arg:
            return [None]
        strings = arg if isinstance(arg, list) else [arg]
        strings = self.tcex.utils.flatten_list([self.tcex.playbook.read(s) for s in strings])
        return strings
